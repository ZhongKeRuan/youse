'use strict'
//vue.config.js
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? "static/myWebSite/" : '',
  /*transpileDependencies: [
    'webpack-dev-server/client',
    'color-string'
  ],*/
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    }
  },
  chainWebpack: config => {
    config
      .entry('index')
      .add("@babel/polyfill")
  },
  devServer: {
    open: true,
    proxy: {
      '/': {
        // target: 'http://10.1.104.112:2081',
        target: 'http://10.1.104.113:8081',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
  }
}