import Vue from 'vue'
import Vuex from 'vuex'
import {Loading} from 'element-ui'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    activeId: 'index',
    activePId: 'index',
    menu: null,
    indexMenu: null,
    activeMenu: [],
    url: '',
    loading: null,
    WbInfo: null,
    DwInfo: null,
    CzqyInfo: null,
    userInfo: null,
    from: null, //上一个路由名称
    activeName1: null,
    activeName2: null,
    showPrint: false,
    permission: 'read',
    permission_hq: 'read',
    buttons: [],
    globalDialog: true,
    jgzInfo: [],
    specialDepartments: null //Object subFlag 工作流初始化的二级部门， specail 特殊发文
  },
  mutations: {
    SET_SHOWPRINT(state, v) {
      state.showPrint = v
    },
    SET_ACTIVEID(state, data) {
      state.activeId = data
    },
    SET_MENU(state, data) {
      state.menu = data;
    },
    SET_ACTIVEMENU (state, data) {
      state.activeMenu = data;
    },
    SET_INDEXMENU (state, data) {
      state.indexMenu = data;
    },
    SET_ACTIVEPID(STATE, DATA) {
      STATE.activePId = DATA
    },
    SET_URL(state, url) {
      state.url = url
    },
    SET_LOADING(state, text) {
      state.loading = Loading.service({fullScreen: true, text, background: 'rgba(255, 255, 255, 0.7)'});
    },
    SET_WbInfo(state, data) {
      state.WbInfo = data
    },
    SET_DwInfo(state, data) {
      state.DwInfo = data
    },
    SET_CzqyInfo(state, data) {
      state.CzqyInfo = data
    },
    SET_USERINFO(state, data) {
      state.userInfo = data
    },
    SET_FROM(state, from) {
      state.from = from
    },
    SET_ACTIVENAME1(state, data) {
      state.activeName1 = data
    },
    SET_ACTIVENAME2(state, data) {
      state.activeName2 = data
    },
    SET_PERMISSION(state, v) {
      state.permission = v
    },
    SET_PERMISSION_HQ(state, v) {
      state.permission_hq = v
    },
    SET_GLOBAL_DIALOG(state, v) {
      state.globalDialog = v
    },
    SET_JGZ(state, v) {
      state.jgzInfo = v
    },
    SET_SPECAIL(state, v) {
      state.specialDepartments = v
    },
    SET_BUTTONS(state, v) {
      state.buttons = v
    },
  },
  actions: {
    getMenuData({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .get('resource/getResource')
          .then(res => {
            if(res) {
              commit('SET_MENU', res.result);
              resolve(res)
            } else {
              reject(res.msg)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    getWbInfo({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .get('jgzinfo/getWbdwInfo')
          .then(res => {
            if(res) {
              commit('SET_WbInfo', res.result);
              resolve(res.result)
            } else {
              reject(res)
            }
          })
          .catch(e => {
            reject(e)
          })
      })
    },
    getDwInfo({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .get('jgzinfo/getDwInfo')
          .then(res => {
            if(res) {
              commit('SET_DwInfo', res.result);
              resolve(res.result)
            } else {
              reject(res)
            }
          })
          .catch(e => {
            reject(e)
          })
      })
    },
    getCzqyInfo({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .get('jgzinfo/getCzqyInfo')
          .then(res => {
            if(res) {
              commit('SET_CzqyInfo', res.result);
              resolve(res.result)
            } else {
              reject(res)
            }
          })
          .catch(e => {
            reject(e)
          })
      })
    },
    getUserInfo({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .get('common/getSessionInfo')
          .then(res => {
            if(res) {
              commit('SET_USERINFO', res.result);
              resolve(res)
            } else {
              reject(res)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    getButtons({commit}) {
      return new Promise((resolve, reject) => {
        vm.axios('gzl/getButton', {params: JSON.parse(vm.$route.query.params || '{}')})
          .then(res => {
            if(res) {
              commit('SET_BUTTONS', res.result)
              resolve(res.result)
            } else {
              reject()
            }
          })
          .catch(err => {
            reject()
          })
      })
    },
    getPermission({dispatch, commit}) {
      return new Promise((resolve, reject) => {
        let from = vm.$route.query.from || 'CaoGao';
        if(from == 'CaoGao') {
          commit('SET_PERMISSION', 'edit')
          commit('SET_PERMISSION_HQ', 'read')
          resolve()
        } else {
          dispatch('getButtons')
            .then(res => {
              /*let r = {
                upload: res.find(v => v.num == 'SBT008') ? 'edit' : 'read',
                hq: res.find(v => v.num == 'SBT017') ? 'edit' : 'read'
              }*/
              commit('SET_PERMISSION', res.find(v => v.num == 'SBT008') ? 'edit' : 'read')
              commit('SET_PERMISSION_HQ', res.find(v => v.num == 'SBT016') ? 'edit' : 'read')
            })
        }
      })
    },
    getJGZInfo({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios('jgzinfo/getOneJgzInfo')
          .then(res => {
            if(res) {
              resolve(res.result)
              commit('SET_JGZ', res.result)
            } else {
              reject(res)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    getSpecailDepartments({commit}) {
      return new Promise((resolve, reject) => {
        Vue.axios('gzl/getSpecialDepartments')
          .then(res => {
            if(res) {
              commit('SET_SPECAIL', res.result)
              resolve(res.result)
            } else {
              reject(res)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  }
})
