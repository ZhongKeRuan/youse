export default props => {
  let fields = {};
  props.forEach((v, i) => {
    (v.show) && (fields[v.label] = v.prop)
  })
  return fields
}