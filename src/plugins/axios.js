"use strict";

import Vue from 'vue';
import axios from "axios";
import {setEncodeUrl} from '../global.js'
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
//let baseURL = process.env.baseURL || process.evn.apiUrl
let HOST = ''
let config = {
  baseURL: process.env.baseURL || process.env.apiUrl || (process.env.NODE_ENV == 'development' ? HOST + '/rest/' : 'rest/') ,
  // headers: {'Access-Control-Allow-Origin': '*'},  //zhangyanhong
  timeout: 60 * 1000, // Timeout
  withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);
_axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    // config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
    if(process.env.NODE_ENV != 'development') config.url = setEncodeUrl(config.url, {timestamp: (new Date()).valueOf()});
    if(config.url && (config.url.includes('workflow/') || config.url.includes('.do'))) {
      // config.baseURL = HOST
      config.baseURL = config.url.includes('.do') ? window.baseUrl : HOST
    }
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    let L = window.vm.$store.state.loading;
    if(L) L.close();
    if(!response.config.url.includes('workflow/') && !response.config.headers['No-Interceptor']) {
      if(response.data.code == 1) {
        return response.data
      } else {
        window.vm.$message.error(response.data.msg || '未知错误')
        return null
      }
    } else {
      return response
    }
  },
  function(error) {
    // Do something with response error
    let L = window.vm.$store.state.loading
    if(L) L.close();
    window.vm.$message.error('网络连接失败！')
    return Promise.reject(error)
  }
);
let Plugin = {}
window.axios = axios;
Plugin.install = function(Vue, options) {
  Vue.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    },
  });
};

Vue.use(Plugin)

export default Plugin;
