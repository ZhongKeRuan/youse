import "@babel/polyfill"
import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import './assets/styles/reset.scss'
import './assets/styles/iconfont/iconfont.css'
import './styles.scss'
import global from './global.js'
(process.env.NODE_ENV == 'development') && (require('./mock/index.js'));
for (let i in global) {
  Vue.prototype[i] = global[i]
}
Vue.use(ElementUI)

Vue.config.productionTip = false

window.vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
