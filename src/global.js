import axios from "axios";
if(!window.console){
    window.console = {
        log: '',
        dir:'',
        warn:''
    }
}
let loginOut = () => {
  localStorage.clear();
  router.replace('/login')
}
function timeStamp () {
  return new Date().valueOf()
}
function setViewTime(str){
  /*
  * 设置返回数据时间显示格式
  * @author zhang_yanhong
   */
  str = str || '';
  if(typeof str != 'string') return str;
  let lastIndex = str.lastIndexOf(':'),
    index = str.indexOf(':');
  return lastIndex == index ? str : str.slice(0, lastIndex)
}
function setEncodeUrl(url, params) {
  let p = [];
  for(let i in params) {
    p.push(i + '=' + params[i])
  }
  let res = url || '';
  res+= res.includes('?') ? '&' : '?';
  res+= p.join('&');
  return res
}
function indexHandleClick(v, from, isNewWindowOpen) {
  /**
  * 待办/已办列表项目点击事件
  * @param v {Object} 当前item的数据
  * @param from {String} 来源 DaiBan DaiYue.....
  * @param isNewWindowOpen {Boolean} 是否新窗口打开， 默认否
  * @return {void}
  * @author cheng_xiaona 2019.4.23
  */
  isNewWindowOpen = isNewWindowOpen || false;
  from = from || 'CaoGao';
  let viewType = 'view';
  let viewStates = ['CaoGao', 'DaiBan'];
  (viewStates.includes(from)) && (viewType = 'edit');
  if(v.isNew == 1 || v.ifNew == 1) {
    let name = null,
      query = {
        from,
        viewType,
        params: null,
        isChuanYue: v.cyType == '传阅' ? 1 : 0,
        fileType: v.poName
      };
    let params = {
      fileTypeId: v.fileTypeId || v.flowType || '',
      id: v.recordId || v.outId || v.inId,
      workitemid: v.id,
      deptId: v.deptId || '',
      sendId: v.id
    };
    let windowSet = '';
    if(query.isChuanYue) {
      isNewWindowOpen = true;
      windowSet = 'width=800,height=600'
    }
    switch(v.poName) {
      case '发文'://发文
        name = 'Publish';
        break;
      case '签报':
        name = 'QianBao';
        break;
      case '白头文':
        name = 'BaiTouWen'
        break;
      case '收文':
        name = 'ShouWen';
        break;
      case '部门收文':
        name = 'ShouWen';
        query.documentType = 'bmsw';
        break
      case '出国审批':
        name = 'abroadApply';
        break
      case '邀请函':
        name = 'Invite';
        break
    }
    query.params = JSON.stringify(params);
    if(isNewWindowOpen) {
      const {href} = this.$router.resolve({name, query});
      let win = window.open(href, '_blank', windowSet);
      return win
    } else {
      this.$router.push({name, query})
    }
  } else {
    if ( from == 'DaiBan') {
      if ( v.typeSort == 'flow' ) {
        let data = {
          "code": v.code,
          "fileTypeId": v.fileTypeId,
          "title": v.title,
          "recordId":v.recordId
        };
        this.axios
          .post('backlog/getFlowTypeUrl',data)
          .then(res => {
            if(res) {
              window.open(res.result)
            }
          })
      } else {
        window.open(v.url || v.viewUrl, '_blank')
      }
    } else {
       window.open(v.url || v.viewUrl, '_blank')
    }
  }
}
function DaiBanHandleClick(v, from, isNewWindowOpen) {
  this.indexHandleClick(v, from, isNewWindowOpen)
}
function getFlowCourse() {
  /**
  * 获取流程跟踪数据并且跳转到该拼接地址
  * @return {Promise}
  * @author cheng_xiaona 2019/4/29 13:22
  */
  let query = JSON.parse(this.$route.query.params);
  let data = {
    "fileTypeId": query.fileTypeId,
    "recordId": query.id
  };
  return new Promise((resolve, reject) => {
    this.axios.post('gzl/getFlowCourse', data)
      .then(res => {
        if(res) {
          resolve(res.result)
        } else {
          reject(res)
        }
      })
      .catch(err => {
        reject(err)
      })
  })
}
function getCommonIdeas(ideafieldname, order) {
  /**
  * 获取所有意见接口
  * @param ideafieldname {String} 意见类型，选填
  * @param order {String} 排序方式, 选填
  * @return {Promise}
  * @author cheng_xiaona 2019/4/29 13:50
  * ideafieldname：
  * 1：领导审批：JTLD_SP_YJ
  * 2：领导审核：JTLD_SH_YJ
  * 3：办公厅审核意见：BGT_LD_YJ
  * 办公核稿意见：
  * 4：会签部门意见：以HQHJ_
  * 5：主办部门意见：以NGHJ_
  */
  ideafieldname = ideafieldname || 'JTLD_SP_YJ';
  order = order || 'ideatime';
  let query = JSON.parse(this.$route.query.params);
  let data = {
    fileTypeId: query.fileTypeId,
    workflowid: query.fileTypeId,
    recordId: query.id,
    order,
    ideafieldname,
  }
  return new Promise((resolve, reject) => {
    this.axios
      .post('idea/getIdea', data)
      .then(res => {
        if(res) {
          resolve(res.result)
        } else {
          reject(res)
        }
      })
      .catch(err => {
        reject(err)
      })
  })
}
function YiFaHandleClick(v) {
  /*
  * 已发 先调后调DaiBanHandleClick
  * @author zhang_yanhong
   */
  let data = {
    "flowType": v.flowType, 
    "id": v.id, 
    "title": v.title
  }
  this.axios
    .post('sent/getSentUrl', data)
    .then(res => {
      if(res) {
        v.url = res.result;
        this.DaiBanHandleClick(v, 'YiFa')
      }
    })
}
function formatePublishFormData(data, type) {
  /**
  * 设置某一对象中某一字段是数组或数组字符串
  * @param data {Object} 被转变的对象
  * @return {Object}
  * @author cheng_xiaona 2019/4/25
  */
  type = type || 'string';
  let routeName = this.$route.name
  let toStringName = []
  let publishStringName = ['zhsDeptId', 'zhsDept', 'chsDeptId', 'chsDept', 'hqDeptId', 'hqDept', 'bgshgUserId', 'bgshgUserName']
  switch(routeName) {
    case 'Publish':
      toStringName = publishStringName
      break;
    case 'QianBao':
      toStringName = publishStringName
      break;
    case 'BaiTouWen':
      toStringName = publishStringName
      break;
    case 'ShouWen':
      toStringName = ['zsldIds','zsldNames','zhsDept','zhsDeptId','xbdwNames','xbdwIds','chsDept', 'chsDeptId', 'yzryNames', 'yzryIds']
      break;
  }
  if(type == 'string') {
    toStringName.forEach(v => {
      data[v] = data[v].join(',')
    })
  } else {
    toStringName.forEach(v => {
      data[v] = data[v].length > 0 ? data[v].split(',') : []
    })
  }
  return data
}
function handleOpenTongZhi(url){
  /*
  * 代开通知详情 外链
  * @author zhang_yanhong
   */
  window.open(url, '_blank');
}
function clearPlaceholders() {
  /**
  * 清空所有placeholder
  * @return {DOMs}
  * @author cheng_xiaona 2019/05/16 15:37
  */
  let allDoms = document.getElementsByTagName('*');
  let res = []
  Array.from(allDoms).forEach(v => {
    let attr = v.getAttribute('placeholder')
    if (attr) {
      v.setAttribute('placeholder', '')
      v.setAttribute('data-placeholder', attr);
      res.push(v)
    }
  })
  return res
}
function copyObj(obj) {
  /**
  * 深拷贝对象
  * @param obj {Object} 被拷贝的对象
  * @return {Object}
  * @author cheng_xiaona 2019/5/19
  */
  return JSON.parse(JSON.stringify(obj))
}
function handleDeleteCaoGao(ids, isBack) {
  isBack = isBack || false;
  return new Promise((resolve, reject) => {
    vm.$confirm(
      '此操作将永久删除, 是否继续?',
      '提示',
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
      .then(() => {
        vm.createLoading('正在操作中...')
        vm.axios
          .post('archivesOut/deleteInfoById', { ids })
          .then(res => {
            if(res) {
              vm.$message({
                type: 'success',
                message: res.msg
              })
              if (isBack) {
                vm.$store.state.from ? vm.$router.back() : vm.$router.replace('/')
              }
              resolve(res)
            } else {
              reject(res)
            }
          })
          .catch(err => {
            reject(err)
          })
      })
      .catch(() => {
        this.$message({
          type: 'info',
          message: '已取消删除'
        })
      });
  })
}
export default {
  copyObj,
  // ShouWen: ['通知','命令','决定','公告','通告','通报','议案','报告','请示','批复','意见','函','会议纪要','决议','公报'],
  ShouWen: ['通知','决议','决定','通报','请示','报告','批复','意见','函','纪要'],
  secretLevel: ['无','商业秘密' ], //密级
  emergencyLevel: ['普通', '紧急', '特急'],//紧急程度
  storageTime: ['短期', '长期', '永久'], //["三年","五年","十年"]保密期限
  loginOut,
  formatePublishFormData,
  createLoading(text){
    this.$store.commit('SET_LOADING', text || '正在执行中...',)
  },
  DaiBanHandleClick,
  indexHandleClick,
  setEncodeUrl,
  setViewTime,
  handleOpenTongZhi,
  getFlowCourse,
  getCommonIdeas,
  YiFaHandleClick,
  timeStamp,
  clearPlaceholders,
  handleDeleteCaoGao,
  isIE9: window.navigator.userAgent.includes('MSIE 9.0')
}
export {setEncodeUrl}