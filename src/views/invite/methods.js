import { config, data } from '../abroadApply/methods.js'
let model = vm.copyObj(config)
let d = vm.copyObj(data)
model.name = 'Invite'
model.data = function() {
  return Object.assign(d, {
      formData: {
      id: null,
      title: null,
      code: null,
      comingNum: null,
      nationality: null,
      comingDate: null,
      reson: null,
      ngDeptName: null,
      ngUserName: null,
      telephone: null,
      ngDate: null,
      stayTime: null
    }
  })
}

export default model
