import publishTemp from '@/views/newWindow/components/publish/publish'
import rightbarTemp from '@/views/newWindow/components/publish/components/rightBarContent'
import LiuChengGenZong from '@/views/publish/LiuChengGenZong.vue'
import ZhiHuiJiLu from '@/views/publish/ZhiHuiJiLu.vue'
/*import opinion from '@/views/publish/components/opinion'*/
import FenFaJiLu from '@/views/publish/FenFaJiLu.vue'
import BanLiJieGuo from '@/views/ShouWen/BanLiJieGuo.vue'
import ShenHeQianPiYiJian from '@/views/publish/components/ShenHeQianPiYiJian'
import methods from '@/views/publish/publish.methods.js'
import { data } from '@/views/publish/publish.data.js'
import createConfig from '@/assets/methods/createConfig.js'
let _methods = {}
for(let i in methods) {
  _methods[i] = methods[i]
}
_methods = Object.assign(_methods, {
  getInitInfo() {
    return new Promise((resolve, reject) => {
      this.axios((this.$route.name == 'Invite' ? 'invitation' : 'abroadApply') + '/create')
        .then(res => {
          if(res) {
            this.$set(this, 'formData', res.result)
            resolve(res.result)
          } else {
            reject(res)
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  }
})
let _data = data({name: 'abroadApply'})
let initConfig = createConfig({
  data: data(),
  methods: _methods,
  components: {
    publishTemp,
    rightbarTemp,
    LiuChengGenZong,
    ZhiHuiJiLu,
    // opinion,
    FenFaJiLu,
    BanLiJieGuo,
    ShenHeQianPiYiJian,
  },
  created() {
    let url = (this.$route.name == 'Invite' ? 'invitation' : 'abroadApply') + '/getInfoById'
    this.init(null, null, url)
  },
  watch: {
    formData: {
      deep: true,
      handler(v) {
        window.formData = v
      }
    }
  }
})
export default initConfig
