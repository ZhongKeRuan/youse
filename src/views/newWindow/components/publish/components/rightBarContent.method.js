import getHistoryButtonState from '@/views/publish/components/getHistoryButtonState.js'
import { mapActions } from 'vuex'
export default {
  ...mapActions(['getButtons']),
  handleBanJieBMSW() {
    this.createLoading('正在办结中...')
    this.axios
      .post('docSend/banJie', this.routeParams)
      .then(res => {
        if(res && res.result == 1) {
          this.$message.success('办结成功！')
          this.$router.replace(this.$store.state.from ? {name: this.$store.state.from } : '/')
        } else {
          this.$message.error(res.msg)
        }
      })
  },
  getIdeaLinks() {
    this.setOpinionDialogVisibale = true;
    if(!this.ideaLinks) {
      this.axios
        .post('idea/getToCollectIdea', this.routeParams)
        .then(res => {
          if(res) {
            this.ideaLinks = res.result
          }
        })
    }
  },
  handleAddIdea() {
    /**
    * 新增意见事件
    * @return {void}
    * @author cheng_xiaona 2019/5/21 17:54
    */
    this.createLoading('正在新增意见中....')
    this.axios
      .post('idea/createNewIdea', {text: this.createIdea})
      .then(res => {
        if(res) {
          this.ideaOptions[1].options = res.result;
          this.createIdea = null;
        }
      })
  },
  handleSortIdea(idea, order) {
    /**
    * 设置自定义意见顺序
    * @param idea {Object} 被排序信息
    * @param order {Number} 排序方式, 默认1, 1 向上排， -1 向下排
    * @return {void}
    * @author cheng_xiaona 2019/5/21 17:35
    */
    order = order || 1
    this.axios
      .post('idea/sortIdeas', {id: idea.id, order})
      .then(res => {
        if(res) {
          this.ideaOptions[1].options = res.result;
        }
      })
  },
  handleDeleteIdea(idea) {
    /**
    * 设置默认意见
    * @param idea {Object} 意见信息
    * @return {void}
    * @author cheng_xiaona 2019/5/21 17:35
    */
    this.createLoading('正在删除....')
    this.axios
      .post('idea/deleteIdea', {id: idea.id})
      .then(res => {
        if(res) {
          this.ideaOptions[1].options = res.result;
        }
      })
  },
  handleSelectFaWenType() {
    this.$set(this.formData, 'ifTsfw', this.extendattr ? '1' : '0');
    this.checkVisible = false;
    this.initWorkData();
  },
  getPrewflevelId() {
    /**
    * 获取上一环节部门节点id
    * @return {void}
    * @author cheng_xiaona 2019/5/17 15:01
    */
    this.axios
      .post('gzl/getPrewflevelId', {workitemid: this.routeParams.workitemid})
      .then(res => {
        if(res) {
          this.PrewflevelId = res.result
        }
      })
  },
  checkFileType() {
    /**
    * 设置工作流发文类型，仅在publish新建时选择
    * @return {void}
    * @author cheng_xiaona 2019/5/16 18:39
    */
  },
  handleBanJie() {
    this.createLoading('正在操作中...')
    this.axios
      .post('gzl/banJie',this.routeParams)
      .then(res => {
        if(res) {
          this.$message.success(res.msg)
          this.$router.back();
        }
      })
  },
  setQianZhang() {
    let d = this.formData;
    window.sign('', d.originText, d.pdfId, 'YZ_ARCHIVES_FILELIST', d.subFlag)
  },
  getZhengWenStatus(fileId) {
    return new Promise((resolve, reject) => {
      this.axios
        .post('office/isExist', {fileId})
        .then(res => {
          if(res.result.isExist) {
            resolve(true)
          } else {
            resolve(false)
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  handleQianZhang() {
    let d = this.formData;
    let fileId = d.originText;
    let pdfId = d.pdfId
    this.createLoading('正在操作中...')
    this.getZhengWenStatus(fileId)
      .then(res => {
        if(res) {
          this.getZhengWenStatus(pdfId)
            .then(res => {
              if(res) {
                this.$confirm(
                  '已经盖过章了，是否重新盖章?',
                  '提示',
                   {
                      confirmButtonText: '确定',
                      cancelButtonText: '取消',
                      type: 'warning'
                    })
                  .then(() => {
                    this.setQianZhang()
                  })
                  .catch(() => {
                    this.$message({
                      type: 'info',
                      message: '已取消盖章'
                    });
                  });
              } else this.setQianZhang();
            })
        } else {
          this.$message.error('请先编辑正文后再进行该操作！')
        }
      })
  },
  handleFenFa(type) {
    let d = this.formData;
    switch(type) {
      case 'SBT006':
        this.axios
          .post('office/isExist', {fileId: this.formData.pdfId})
          .then(res => {
            if(res.result.isExist) {
              window.selectSendAllNew('', '', 0, d.creUserDeptId, '', d.outWord || d.inWord, '001061', '', this.routeParams.id, '', 'FW');
            } else {
              this.$message.warning('您还未盖章，请先进行盖章！')
            }
          })
        break;
      default:
        window.selectSendAllNew('', '', 0, d.creUserDeptId, '', d.outWord || d.inWord, '001061', '', this.routeParams.id, '', 'FW');
        break
    }
  },
  handleJiaoBan() {
    let d = this.formData;
    window.jiaoban('', '', 0, d.creUserDeptId, '', d.outWord || d.inWord || d.taskCodeWord || d.code, '001061', '', this.routeParams.id, '', 'FW', d.taskCodeWord || d.code);
  },
  loadZhiHuiNode(node, resolve) {
    if(node.level == 0) {
      this.axios
        .get('jgzinfo/getChildrenNodes')
        .then(res => {
          if(res) {
            resolve(res.result)
          }
        })
    } else {
      this.axios
        .post('jgzinfo/getChildrenBySend', node.data)
        .then(res => {
          if(res) {
            resolve(res.result);
          }
        })
    }
  },
  handleSubmitChuanYue(vCheck) {
    let arr = vCheck;
    if(arr == null) {
      return;
    }
    this.createLoading('正在办理中...');
    let data = {
      id: JSON.parse(this.$route.query.params || '{}').id || this.formData.outId || this.formData.inId || this.formData.id,
      title: this.formData.title,
      wjType: this.formData.wjType,
      userInfo: [],
      isChuanYue: this.$route.query.documentType == 'bmsw' ? 1 : (this.$route.query.isChuanYue || '0')
    }
    arr.forEach(v => {
      data.userInfo.push({id: v.ownid, name: v.ownname})
    })
    return new Promise((resolve, reject) => {
      this.axios
        .post('docSend/chuanYue', data)
        .then(res => {
          if(res) {
            this.$message.success(res.result);
            (window.refreshFenFaJiLu != void(1)) && (window.refreshFenFaJiLu());
            resolve(res)
          } else {
            reject(res.msg)
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  getButtonName(num) {
    return this.buttons.find(v => v.num == num).buttonname
  },
  dataValided(res, fn) {
    res = res || {}
    let msg = this.trim(res.result.exception);
    if (res && res.result && msg.length == 0) {
      fn()
      return true
    }
    else {
      this.$notify({
        duration: 30000,
        type: 'error',
        message: msg,
        title: '错误'
      });
      return false
    }
  },
  trim(str) {
    return str.replace(/\s+/g, '')
  },
  handleSave(isFirst) {
    return new Promise((resolve, reject) => {
      this.baseForm.validate((v, prop) => {
        console.log('1111111', prop, v)
        if(v || isFirst) {
          let RN = this.$route.name;
          let url = 'archivesOut/save'
          switch(RN) {
            case 'ShouWen':
              url = 'archivesIn/save';
            break;
            case 'Invite':
              url = 'invitation/save'
            break
            case 'abroadApply':
              url = 'abroadApply/save'
            break
          }
          let args = [
            Object.assign({}, JSON.parse(JSON.stringify(this.formData))),
            'string'
          ];
          let data = this.formatePublishFormData(...args);
          delete data.historyButtonShow;
          this.createLoading('正在保存中...')
          this.axios
            .post(url, data)
            .then(res => {
              if (res) {
                let data = this.formatePublishFormData(res.result, 'array')
                this.flag = false;
                this.$emit('update:formData', data);
                this.$message.success('保存成功！')
                resolve();
                if(this.firstInitWork) {
                  this.$nextTick(() => {
                    this.initWorkData();
                  })
                }
                this.getHistoryButtonState(res.result.originText)
                  .then(data => this.$emit('update:historyButtonShow', data))
              } else {
                reject()
              }
            })
        } else {
          for(let i in prop) {
            this.$notify.warning({
              title: '提示',
              message: prop[i][0].message
            })
          }
          reject()
        }
      })
    })
  },
  handleSetIdea() {
    this.$emit('update:formData', Object.assign(this.formData, {idea: this.idea}))
  },
  setDilogSatatus(treeData) {
    const ti = this.workStreamData.node[0].examinetag.find(v => v.type == 5);//5 同时办 6顺序办 4 共同办（竞争办）
    this.examinetag = (ti || this.workStreamData.node[0].examinetag[0] || this.workStreamData.node[0].examinetag.find(v => v.selected == 'true')).type;
    if (treeData[0].nodes.length == 1 && ((treeData[0].nodes[0].nodes && treeData[0].nodes[0].nodes.length == 1) || !treeData[0].nodes[0].nodes)) {
      this.memberChecked = treeData;
      this.handleSubmitMember(true)
    } else {
      treeData.forEach(parent => {
        parent.deptId = 0;
        parent.nodes.forEach(child => {
          child.deptId = parent.id
        })
      })
      this.treeData = treeData;
      this.examinetagRadioData = this.workStreamData.node[0].examinetag;
      this.dialogVisible = true;
    }
  },
  setWorkFlowDataAttr(data) {
    /**
    * 设置最后一步工作流提交时候的传参
    * @param data {JSON} 工作流数据
    * @return {void}
    * @author cheng_xiaona 2019/8/27
    */
    (this.formData.hqDeptId) && (data.subflowlist = data.subflowdeptlist = this.formData.hqDeptId.join(','));
    (this.formData.needGrade) && (data.attr = this.formData.needGrade);
    data.title = this.formData.title;
    data.idea = this.submitIdea;
  },
  handleSubmit() {
    /**
    * 下一环节提交点击事件
    * @return {void}
    * @author cheng_xiaona 2019/5/9
    */
    this.handleSave()
      .then(() => {
        this.optionData = this.optionData || JSON.parse(JSON.stringify(this.workStreamData))
        if(this.optionData.flag == 'flowinstance') {
          this.setDilogSatatus(this.initTreeData(this.copyObj(this.workStreamData.node)))
        } else {
          if(this.optionData.ifSameTime != 1) {
            this.optionData.node = [this.nextNode];
          }
          /*if(this.formData.hqDeptId) {
            this.optionData.subflowlist = this.optionData.subflowdeptlist = this.formData.hqDeptId.join(',');
          }
          (this.formData.needGrade) && (this.optionData.attr = this.formData.needGrade);
          this.optionData.title = this.formData.title;
          this.optionData.idea = this.submitIdea;*/
          this.setWorkFlowDataAttr(this.optionData)
          this.createLoading('提交中....');
          this.axios
            .post('gzl/encodeInfo', this.optionData)
            .then(res => {
              this.geWorkTime++;
              this.dataValided(res, () => {
                this.memberData = res.result.node;
                this.workStreamData = res.result;
                if(res.result.flag == 1) {
                  let result = res.result.node[0]
                  this.setState()
                  this.$message({
                    dangerouslyUseHTMLString: true,
                    message: '<p style="line-height: 2em">下一办理环节:' + result.opName +'<br />下一办理人：' + result.receiveName + '</p>',
                    duration: 5000,
                    type: 'success',
                    showClose: true,
                  });
                  this.$router.replace('/')
                } else {
                  this.setDilogSatatus(this.initTreeData(res.result.node))
                }
              })
            })
        }
      })
  },
  handleSubmitMember(isSingle) {
    /**
    * 人员选择后提交事件
    * @param isSingle {Boolean} 是否只有一个人，选填默认否
    * @return {void}
    * @author cheng_xiaona
    * 选择人员：注意：remind改成对象；limit改成对象；examinetag改成一个值，多的人员删除
    **/
    if(this.$refs.tree && this.$refs.tree.getCheckedNodes().length == 0 && !isSingle) {
      this.$message.error('请选择参与者！')
      return;
    }
    this.createLoading('提交中....');
    let node = this.copyObj(this.workStreamData.node)
    if(!isSingle) {
      node[0].nodes = this.formateTreeData(this.copyObj(this.workStreamData.node[0].nodes));
    }
    node.forEach(v => {
      v.remind = v.remind[0];
      v.limit = Object.assign({}, v.limit);
      v.examinetag = this.examinetag;
    });
    /*(this.formData.hqDeptId) && (this.workStreamData.subflowlist = this.workStreamData.subflowdeptlist = this.formData.hqDeptId.join(','));
    (this.formData.needGrade) && (this.workStreamData.attr = this.formData.needGrade);
    this.workStreamData.title = this.formData.title;
    this.workStreamData.idea = this.submitIdea;*/
    this.setWorkFlowDataAttr(this.workStreamData)
    this.workStreamData.node = node;
    this.axios
      .post('gzl/encodeInfo', this.workStreamData)
      .then(res => {
        this.geWorkTime++;
        this.dataValided(res, () => {
          this.setState()
          let result = res.result.node[0]
          this.$message({
            dangerouslyUseHTMLString: true,
            message: '<p style="line-height: 2em">下一办理环节:' + result.opName +'<br />下一办理人：' + result.receiveName + '</p>',
            duration: 5000,
            type: 'success',
            showClose: true,
          });
          this.$router.replace('/')
        })
      })
  },
  setState() {
    let url = ''
    switch(this.$route.name) {
      case 'ShouWen':
        url = 'archivesIn';
      break;
      case 'Invite':
        url = 'invitation'
      break
      case 'abroadApply':
        url = 'abroadApply'
      break
      default:
        url = 'archivesOut'
    }
    url += '/updateSubFlagById'
    let params = {
      id: this.formData.outId || this.formData.inId || this.formData.id,
      subFlag: '01'
    }
    this.axios
      .post(this.setEncodeUrl(url, params))
  },
  handleGoToWorkStream() {
    let data = this.initWorkFlowData();
    data.ifSameTime = '1'
    let params = JSON.stringify(data);
    let fileName = 'archivesOut'
    switch(this.$route.name) {
      case 'ShouWen':
        fileName = 'archivesIn';
      break;
      case 'Invite':
        fileName = 'invitation'
      break
      case 'abroadApply':
        fileName = 'abroadApply'
      break
    }
    this.$router.push({
      name: 'workStream',
      query: {
        params,
        id: this.formData.outId || this.formData.inId || this.formData.id,
        fileName
      }
    })
  },
  initWorkFlowData() {
    /**
    * 初始化工作流提交数据
    * @return {void}
    * @author cheng_xiaona 2019/7/9 10:59
    */
    let data = {
      "sysid": "1",
      "flag": "selectoption",
      "filetypeid": '',
      "title": '',
      "recordid": '',
      "deptid": '',
      "userid": '',
      "docid": '',
      "workitemid": "",
      "idea": "",
      "attr": "",
      "attr1": "",
      "attr2": "",
      /*"subflowlist": (this.formData.hqDeptId && this.formData.hqDeptId.join(',')) || '',
      "subflowdeptlist": (this.formData.hqDeptId && this.formData.hqDeptId.join(',')) || '',*/
      "subflowlist": "",
      "subflowdeptlist": "",
      "appointname": "",
      "userlist": "",
      "covermode": "",
      "ifSameTime": '0',
      "extendattr": ''
    }
    let option = {
      "filetypeid": 'flowType',
      // "title": 'title',
      "recordid": 'outId',
      "docid": 'outId',
      "userid": 'creUserId',
      "deptid": 'creUserDeptId',
      // "attr": 'needGrade'
    }

    let _option = {};
    switch(this.$route.name) {
      case 'ShouWen':
        _option = {
          recordid: 'inId',
          docid: 'inId'
        }
      break;
      default:
        if(this.formData.id) {
          _option = {
            recordid: 'id',
            docid: 'id',
            /*deptid: 'creDeptId',
            filetypeid: 'flowId',*/
          }
        }
      break
    }
    option = Object.assign(option, _option)
    if(this.viewType == 'edit') {
      data.workitemid = JSON.parse(this.$route.query.params).workitemid
    }
    // data.idea = this.submitIdea;
    for(let i in option) {
      data[i] = this.formData[option[i]]
    }
    if(this.$route.query.from == 'DaiBan') {
      data.deptid = JSON.parse(this.$route.query.params).deptId
    }
    let a = [];
    if(this.$route.name == 'Publish') {
      (this.formData.ifTsfw == '1') && (a.push('subInfo=2'));
    }
    let depts = this.$store.state.specialDepartments.subFlag;
    /*(depts.includes(this.$store.state.userInfo.deptId)) && (a.push('subisFlag=2'));
    (a.length == 0) && (a.push('subisFlag=1'));*/
    a.push('subisFlag=' + (depts.includes(this.$store.state.userInfo.deptId) ? '2' : '1'))
    data.extendattr = a.join(',');
    return data
  },
  initWorkData() {
    /**
    * 工作流初始化
    * 勿动
    * 轻易勿动
    * 动前一定要备份， 一定要备份
    * 这个接口有毒
    * @return {void}
    * @author cheng_xiaona 2019.05.09 21:11
    */
    this.flag = false;
    let data = this.initWorkFlowData()
    this.createLoading('提交中....');
    return new Promise((resolve, reject) => {
      this.axios
        .post('gzl/encodeInfo', data)
        .then(res => {
          this.geWorkTime++;
          if(res.result.ifSameTime == 1) {
            this.workStreamData = res.result;
            resolve()
          } else {
            let status = this.dataValided(res, () => {
              this.radioData = res.result.node;
              this.nextNode = this.radioData[0];
              this.workStreamData = res.result;
              this.firstInitWork = false
            });
            status ? resolve(res) : reject(res);
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  initTreeData(treeData) {
    Array.prototype.initSelected = function() {
      this.forEach(v => {
        v.selected = false;
        v.order = 0;
        if(v.nodes) v.nodes.initSelected();
      })
    };
    treeData.initSelected();
    return treeData
  },
  createTreeData() {
    let cloneData = JSON.parse(JSON.stringify(this.$refs.tree.getCheckedNodes().concat(this.$refs.tree.getHalfCheckedNodes()))) // 对源数据深度克隆
    return cloneData.filter(father => {
      let branchArr = cloneData.filter(child => father.id == child.deptId) //返回每一项的子级数组
      branchArr.length > 0 ? father.nodes = branchArr : '' //如果存在子级，则给父级添加一个children属性，并赋值
      return father.deptId == 0; //返回第一层
    });
  },
  formateTreeData(treeData) {
    let res = [];
    let checkedNodes = JSON.parse(JSON.stringify(this.$refs.tree.getCheckedNodes().filter(v => !v.nodes)));
    checkedNodes.sort((a, b) =>  b.order - a.order)
    Array.prototype.getParentNodes = function(node, parent) {
      this.forEach(v => {
        let p = JSON.stringify(v)
        let c = JSON.stringify(node);
        if(p.includes(c) && p != c) {
          let item = JSON.parse(p)
          item.nodes = [];
          parent.push(item);
          v.nodes.getParentNodes(node, item.nodes)
        } else if(p == c) {
          parent.push(node)
        }
      })
    }
    checkedNodes.forEach(v => {
      treeData.getParentNodes(v, res)
    })
    Array.prototype.clearOrder = function() {
      this.forEach(v => {
        delete v.order;
        (v.nodes) && (v.nodes.clearOrder())
      })
    }
    res.clearOrder();
    console.log(res)
    return res
  },
  /*setParentNodeSelected(treeData, data, val) {
    let id = data.deptId;
    Array.prototype.initParentNodeStatus = function(id, nodes, val) {
      this.forEach(v => {
        if(v.id == id) {
          if(val == 'true') {
            v.selected = val;
            v.order = this.order
          } else {
            v.selected = (v.nodes && v.nodes.some(v => v.selected));
            v.order = 0
          }
          if(v.deptId) {
            nodes.initParentNodeStatus (v.deptId, nodes, val)
          }
          return;
        } else if(v.nodes) {
          v.nodes.initParentNodeStatus (id, nodes, val)
        } else {
          return
        }
      })
    }
    treeData.initParentNodeStatus(id, treeData, val);
    return treeData
  },*/
  checkChange(data, status, childStatus) {
    /**
    * data 属性的数组中该节点所对应的对象、节点本身是否被选中、节点的子树中是否有被选中的节点
    * @param data {Object} 属性的数组中该节点所对应的对象
    * @param status {Boolean} 节点本身是否被选中
    * @param childStatus {Boolean} 节点的子树中是否有被选中的节点
    * @return {void}
    * @author cheng_xiaona 2019/4/20
    */
    if(status) {
      data.order = Number(this.order);
      this.order--
    } else {
      data.order = 0
    }
    let arr = JSON.parse(JSON.stringify(this.$refs.tree.getCheckedNodes()));
    arr = arr.filter(v => !v.nodes);
    arr.sort((a, b) => b.order-a.order)
    let textArr = [];
    arr.forEach(v => textArr.push(v.name))
    this.memberText = textArr.join('/');
  },
  getIdeas () {
    this.axios
      .get('idea/getCommonIdea')
      .then(res => {
        if(res) {
          this.ideaOptions[1].options = res.result
        }
      })
  },
  setName(name) {
    return name.replace('同时办', '并行办')
  },
  getHistoryButtonState
}