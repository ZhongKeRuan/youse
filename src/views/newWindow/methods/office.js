import axios from 'axios'
function trim(str) {
  return str.replace(/\s+g/, '')
}
/**
 * 进行套红
 * @param contextPath
 * @param tempId
 * @param textId
 * @param taoHongId
 * @param modelId
 * @param recordId
 * @param fileTypeId 
 * @return
 */
function taoHong(contextPath, tempId, textId, taoHongId, modelId, recordId, tableName, fileTypeId) {
  axios.get(contextPath + '/plugin/office/jsp/getBookJson.jsp?modelId=' + modelId + '&recordId=' + recordId + '&fileTypeId=' + fileTypeId)
    .then(res => {
      // data = trim(data);
      setTimeout("", 5000);
      let data = res.data
      taoHongDoc(contextPath, tempId, textId, taoHongId, data, recordId, tableName);
    })
  /*jQuery.get(contextPath + '/plugin/office/jsp/getBookJson.jsp?modelId=' + modelId + '&recordId=' + recordId + '&fileTypeId=' + fileTypeId, function(data) {
    data = trim(data);
    setTimeout("", 5000);
    taoHongDoc(contextPath, tempId, textId, taoHongId, data, recordId, tableName);
  });*/
}

/**
 * 带有正文的套红
 * @param contextPath 上下文路径
 * @param tempId 套红模板的id，即在filelist表中的filename值。
 * @param textId 正文的id，即在filelist表中的filename值。
 * @param taoHongId 套红后正文存储的id
 * @param json 其他要替换的书签，以json的形式出现，程序会遍历这些kv值，替换相应的书签。
 * @param recordId 业务记录id
 * @param tablename 要查询的表的Id
 * @return 没有返回值
 */
function taoHongDoc(contextPath, tempId, textId, taoHongId, json, recordId, tableName) {
  var templateUrl = contextPath + "/plugin/office/jsp/loadDoc.jsp?docType=0&id=" + tempId + "&tableName=" + tableName;
  var textUrl = contextPath + "/plugin/office/jsp/loadDoc.jsp?docType=0&id=" + textId + "&tableName=" + tableName;
  var fileSaveUrl = contextPath + "/plugin/office/jsp/saveDoc.jsp?docType=0&id=" + taoHongId + "&tableName=" + tableName;
  var affixUrl = contextPath + "/plugin/office/jsp/loadAffixDoc.jsp?docType=0&id=" + recordId;
  json = json == null || json == undefined ? "" : json;

  var para = {
    templateUrl: templateUrl,
    textUrl: textUrl,
    otherData: json,
    FileId: taoHongId,
    saveUrl: fileSaveUrl,
    affixUrl: affixUrl
  };
  submitWithForm(contextPath + "/plugin/office/taohongoffice.jsp", para);
}

/**
 *  提交数据
 * @url  连接
 * @params 参数
 */
function submitWithForm(url, params) {
  var temp = document.createElement("form");
  temp.action = url;
  temp.method = "post";
  temp.target = "_blank";
  temp.style.display = "none";
  for (var x in params) {
    var opt = document.createElement("textarea");
    opt.name = x;
    opt.value = params[x];
    temp.appendChild(opt);
  }
  document.body.appendChild(temp);
  temp.submit();
  return temp;
}

/**
 * 编辑方式打开一个已经存在的文档
 * @param contextPath 上下文路径
 * @param id 文档的id，即在filelist表中的filename值。
 * @tableName  表名
 * @return 没有返回。
 */
function openDoc(contextPath, id, tableName) {
  var templateUrl = contextPath +
    "/plugin/office/jsp/loadDoc.jsp?docType=0&id=" + id + "&tableName=" + tableName;
  var fileSaveUrl = contextPath +
    "/plugin/office/jsp/saveDoc.jsp?docType=0&id=" + id + "&tableName=" + tableName;

  var fileUrl = templateUrl;
  var FileId = id;
  var saveUrl = fileSaveUrl;

  var linkUrl = contextPath + "/plugin/office/editoffice.jsp?subflag=" + subflag + "&fileUrl=" + fileUrl + "&FileId=" + FileId + "&saveUrl=" + saveUrl + "&tableName=" + tableName;
  window.open(linkUrl, '正文', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 30) + ',top=0,left=0,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no');

}

/**
 * 只读方式打开一个已经存在的文档
 * @param contextPath 上下文路径
 * @param id 文档的id，即在filelist表中的filename值。
 * @return 没有返回。
 */
function readDoc(contextPath, id, tableName) {
  var templateUrl = contextPath +
    "/plugin/office/jsp/loadDoc.jsp?docType=0";

  var fileUrl = templateUrl;
  var FileId = id;

  var linkUrl = contextPath + "/plugin/office/readoffice.jsp?fileUrl=" + fileUrl + "&FileId=" + FileId + "&tableName=" + tableName;
  window.open(linkUrl, '正文', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 30) + ',top=0,left=0,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no');

}
export default {
  taoHong,
  openDoc
}