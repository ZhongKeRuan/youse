let infoTypeOptions = [],
  ifbackOptions = [],
  stateOptions = [],
  treeData,
  treeManData;
let wjType = {
  label: '文件类型',
  width: '33%',
  prop: 'wjType',
  type: 'select',
  options: [
    {
      label: '全部',
      value: 'all'
    },
    {
      label: '发文',
      value: 'fw'
    },
    {
      label: '白头文',
      value: 'btw'
    },
    {
      label: '签报',
      value: 'qb'
    },
    {
      label: '收文',
      value: 'sw'
    },
    {
      label: '部门收文',
      value: 'bmsw'
    },
    {
      label: '其他',
      value: 'qt'
    }
  ]
}
let data = {
  searchBar: null,
  search: {
    YiBan: {
      form: {
        title: null,
        startTime: null,
        endTime: null,
        wjType: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
        wjType
      ]
    },
    DaiBan: {
      form: {
        title: null,
        startTime: null,
        endTime: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
      ]
    },
    message: {
      form: {
        title: null,
        startTime: null,
        endTime: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
      ]
    },
    DaiYue: {
      form: {
        title: null,
        startTime: null,
        endTime: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
      ]
    },
    CaoGao: {
      form: {
        title: null,
        startTime: null,
        endTime: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
      ]
    },
    TongZhi: {
      form: {
        title: null,
        startTime: null,
        endTime: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
      ]
    },
    YiYue: {
      form: {
        title: null,
        startTime: null,
        endTime: null,
        wjType: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
        wjType
      ]
    },
    YiFa: {
      form: {
        title: null,
        startTime: null,
        endTime: null,
        wjType: null
      },
      items: [{
          label: '标题',
          width: '33%',
          prop: 'title',
        },
        {
          label: '开始时间',
          width: '33%',
          prop: 'startTime',
          type: 'datePicker'
        },
        {
          label: '结束时间',
          width: '33%',
          prop: 'endTime',
          type: 'datePicker'
        },
        wjType
      ]
    }
  },
  columns: {
    TongZhi: [{
        prop: 'title',
        label: '标题',
        'min-width': 150,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'issueTime',
        label: '创建时间',
        width: 130,
        show: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.issueTime)
        }
      }
    ],
    DaiBan: [
      {
        prop: 'poName',
        label: '待办类型',
        width: 120,
        show: true,
        sortable: true
      },
      {
        prop: 'title',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'preUserName',
        label: '上一办理人',
        width: 130,
        show: true,
        sortable: true
      },
      {
        prop: 'preTime',
        label: '接收时间',
        width: 150,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.preTime)
        }
      },
    ],
    message: [
      {
        prop: 'poName',
        label: '待办类型',
        width: 120,
        show: true,
        sortable: true
      },
      {
        prop: 'title',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'preUserName',
        label: '上一办理人',
        width: 130,
        show: true,
        sortable: true
      },
      {
        prop: 'preTime',
        label: '接收时间',
        width: 150,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.preTime)
        }
      },
    ],
    YiBan: [{
        prop: 'fileTypeName',
        label: '类型',
        width: 150,
        show: true,
        sortable: true
      },
      {
        prop: 'title',
        label: '标题',
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'preTime',
        label: '办理时间',
        width: 150,
        show: true,
        tooltip: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.preTime)
        }
      }
    ],
    YiYue: [{
        prop: 'sendTitle',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'creTime',
        label: '创建时间',
        width: 150,
        show: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.creTime)
        }
      },
      {
        prop: 'poName',
        label: '类型',
        width: '100',
        show: true,
        sortable: true
      },
    ],
    DaiYue: [{
        prop: 'title',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'poName',
        label: '类型',
        width: '100',
        show: true,
        sortable: true
      },
      /*{
        prop: 'poName',
        label: '承办部门',
        width: 120,
        show: true,
        sortable: true
      },*/
      {
        prop: 'preUserName',
        label: '承办人',
        width: 100,
        show: true,
        sortable: true
      },
      {
        prop: 'preTime',
        label: '接收时间',
        width: 150,
        show: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.preTime)
        }
      },
    ],
    CaoGao: [{
        prop: 'title',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'wjType',
        label: '类型',
        width: '100',
        show: true,
        sortable: true
      },
      {
        prop: 'creDate',
        label: '创建时间',
        width: 150,
        show: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.creDate)
        }
      },
    ],
    YiFa: [{
        prop: 'title',
        label: '标题',
        minWidth: 200,
        show: true,
        showOverflowTooltip: true,
        tooltip: true,
        sortable: true
      },
      {
        prop: 'creDate',
        label: '创建时间',
        width: 150,
        show: true,
        sortable: true,
        createTemp(scope) {
          return vm.setViewTime(scope.row.creDate)
        }
      },
    ]
  },
  urls: {
    TongZhi: 'news/getNewsInfo',
    DaiBan: 'backlog/getBackLogByPage',
    YiBan: 'finished/getFinishedByPage',
    YiYue: 'afterread/getAfterReadInfoByPage',
    DaiYue: 'pendread/getPendReadInfoByPage',
    CaoGao: 'archivesOut/getCgInfo',
    YiFa: 'sent/getSentByPage',
    message: 'message/getMoreMessageList'
  },
  modelsData: [
    {
      tabName: '公文审批',
      list: [
        {
          name: '发文',
          url: 'Publish',
          isNew: '1'
        },
        {
          name: '收文',
          url: 'ShouWen',
          isNew: '1'
        },
        {
          name: '签报',
          url: 'QianBao',
          isNew: '1'
        },
        {
          name: '白头文',
          url: 'BaiTouWen',
          isNew: '1'
        },
        {
          name: '邀请函审批表单',
          url: 'Invite',
          isNew: '1'
        },
        {
          name: '出国(赴港澳)任务及人员审查审批单',
          url: 'abroadApply',
          isNew: '1'
        }
      ]
    }
  ],
  operating: {
    CaoGao: [
      {
        style: {
          icon: 'el-icon-delete-solid',
          type: 'primary'
        },
        text: '删除',
        handler(i, data, scope, _this) {
          _this.handleDeleteCaoGao([{id: scope.row.outId || scope.row.inId, fileTypeId: scope.row.flowType}])
            .then(res => {
              _this.createData()
            })
        }
      }
    ]
  },
  slectionOprating: [
    {
      style: {
        icon: 'el-icon-delete-solid',
        type: 'primary'
      },
      text: '删除',
      handler(data, _this) {
        let params = [];
        data.forEach((v, i) => {
          params.push({
            id: v.outId || v.inId,
            fileTypeId: v.flowType
          })
        })
        _this.handleDeleteCaoGao(params)
          .then(res => {
            _this.createData()
          })
      }
    }
  ]
}
export {
  data
}