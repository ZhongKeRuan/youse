export default {
  '标题': {
    width: 50,
    prop: 'title'
  },
  '日期': {
    width: 50,
    prop: 'time'
  },
  '流程状态': {
    width: 50,
    prop: 'status'
  },
  '文号': {
    width: 50,
    prop: 'ids'
  },
  '拟稿日期': {
    width: 50,
    prop: 'start'
  },
  '发文日期': {
    width: 50,
    prop: 'publishTime'
  },
  '拟稿部门': {
    width: 50,
    prop: 'department1'
  },
  '签发人': {
    width: 50,
    prop: 'singrate'
  },
  '主送领导': {
    width: 50,
    prop: 'leader'
  },
  '主送单位': {
    width: 50,
    prop: 'firstUnit'
  },
  '抄送单位': {
    width: 50,
    prop: 'units'
  },
  '附件名称': {
    width: 50,
    prop: 'appatchment'
  },
  '收文编号': {
    width: 50,
    prop: 'id'
  },
  '收文类型': {
    width: 50,
    prop: 'types'
  },
  '来文单位': {
    width: 50,
    prop: 'fromUnit'
  },
  '来文字号': {
    width: 50,
    prop: 'fromIds'
  },
  '来文日期': {
    width: 50,
    prop: 'fromTime'
  },
  '主办单位': {
    width: 50,
    prop: 'firstUnits'
  },
  '协办单位': {
    width: 50,
    prop: 'secondUnits'
  },
  '阅知单位': {
    width: 50,
    prop: 'witchUnits'
  },
  '阅知人员': {
    width: 50,
    prop: 'readPeople'
  },
  '编号': {
    width: 50,
    prop: 'id'
  },
  '拟稿单位': {
    width: 50,
    prop: 'unit2'
  },
  '拟稿人': {
    width: 50,
    prop: 'name1'
  },
  '呈送领导': {
    width: 50,
    prop: 'leaderName'
  },
  '阅知部门': {
    width: 50,
    prop: 'unit1'
  },
}