export default (id) => {
  return new Promise((resolve, reject) => {
    vm.axios
      .post('office/isHasVersion', {id})
      .then(res => {
        resolve(res.result)
      })
      .catch(err => {
        reject(err)
      })
  })
}