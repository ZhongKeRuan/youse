let formData = {
  outId: null,
  outCode: null,
  year: null,
  outNum: null,
  needGrade: '普通',
  secretGrade: '无',
  secretDate: null,
  outType: '无',
  title: null,
  bgshgUserId: [], //主送领导
  bgshgUserName: [], //主送领导
  hqDeptId: [],
  hqDept: [],
  draftsmanName: '',
  ngDeptName: '',
  ngDeptId: '',
  draftsmanId: [],
  ngDate: null,
  fwDate: null,
  zhsDeptId: [],
  zhsDept: [],
  chsDeptId: [],
  chsDept: [],
  originText: null,
  affixId: null,
  affixName: null,
  memo: null,
  flowType: null,
  contactUser: null,
  contactPhone: null,
  subflag: null,
  trueText: null,
  linkSignId: null,
  creDate: null,
  creUserId: null,
  creUserName: null,
  creUserDeptId: null,
  creUserDeptName: null,
  idea: null,
  fileType: '',
  historyButtonShow: false,
  finishLimit: null
}
let data = () => {
  return {
    tempTitle: '公文审批单',
    rightBarShow: false,
    from: 'CaoGao',
    baseForm: null,
    checkValidate: false,
    formValidate: false,
    viewType: 'create', //视图类型：create新增 edit 编辑 view 查看
    toolBar: [
      /*{
        title: '审批意见',
        id: 'shqpyj',
        icon: 'el-icon-edit',
      },
      {
        title: '会签部门意见',
        id: 'bmhq',
        icon: 'el-icon-edit'
      },
      {
        title: '主办部门意见',
        id: 'zbbmyj',
        icon: 'el-icon-edit'
      },
      {
        title: '办公厅意见',
        id: 'bgtsh',
        icon: 'el-icon-edit'
      },*/
      {
        title: '审核签批意见',
        id: 'shqpyj',
        icon: 'el-icon-date'
      },
      {
        title: '流程跟踪',
        id: 'lcgz',
        icon: 'el-icon-date'
      },
      {
        title: '分发记录',
        id: 'ffjl',
        icon: 'el-icon-date'
      },
      {
        title: '知会记录',
        id: 'cyjl',
        icon: 'el-icon-date'
      }
    ],
    ideaItems: {},
    // ideaItems: {
    //   "审批意见": [
    //     /*{
    //       text: '领导审批',
    //       code: 'JTLD_SP_YJ',
    //       data: []
    //     },*/
    //     {
    //       text: '领导审核',
    //       code: 'JTLD_SH_YJ',
    //       data: []
    //     },
    //     /*{
    //       text: '领导会签',
    //       code: 'HQHJ_',
    //       data: []
    //     },*/
    //   ],
    //   "办公厅意见": [{
    //       text: '审核意见',
    //       code: 'BGT_LD_YJ',
    //       data: [],
    //     },
    //     /*{
    //       text: '核稿意见',
    //       code: 'BGT_HG_YJ',
    //       data: []
    //     },*/

    //   ],
    //   "部门会签": [{
    //       text: '部门会签',
    //       code: 'HQHJ_',
    //       data: null,
    //       deptShow: true
    //     },

    //   ],
    //   "主办部门意见": [{
    //       text: '主办部门',
    //       code: 'NGHJ_',
    //       data: null,
    //       deptShow: true,
    //       noteShow: true
    //     },

    //   ]
    // },
    formData: JSON.parse(JSON.stringify(formData)),
    loading: true,
    baseForm: {},
    historyButtonShow: false
  }
}
export {
  formData,
  data
}