import getHistoryButtonState from '@/views/publish/components/getHistoryButtonState.js'
let methods = {
  getInitInfo(wjType) {
    /*
     * 获取初始化信息
     * @params jsonData
     */
    let url = 'archivesOut/create';
    return new Promise((resolve, reject) => {
      this.axios
        .get(url, { params: { wjType } })
        .then((res) => {
          if (res) {
            this.formkData = Object.assign(this.formData, res.result);
            resolve(res.result)
          } else {
            reject()
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  getOpinions(res) {
    /**
    * 获取所有意见数据
    * @param res {Objec} formData数据
    * @return {void}
    * @author cheng_xiaona 2019/5/15 14:55
    */
    for(let i in this.ideaItems) {
      this.ideaItems[i].forEach(v => {
        let data = {
          fileTypeId: res.flowType,
          // workflowid: res.flowType,
          recordId: res.outId || res.inId,
          order: "ideatime",
          ideaFieldName: v.code,
          ideaTitle: v.text
        }
        if(v.ideaName) {
          for (let n in v.ideaName) {
            data[n] = res[v.ideaName[n]]
          }
        }
        data.workflowid = data.fileTypeId
        this.axios
          .post('idea/getIdea', data)
          .then(res => {
            if(res) {
              this.setOpinion(v.code, res.result)
            }
          })
      })
    }
  },
  setOpinion(code, data) {
    for(let index in this.ideaItems) {
      let i = this.ideaItems[index].findIndex(v => v.code == code);
      if(i != -1) {
        this.ideaItems[index][i].data = data;
        break;
      }

    }
  },
  getFormData(url) {
    url = url || 'archivesOut/getInfoById'
    let params = JSON.parse(this.$route.query.params)
    return new Promise((resolve, reject) => {
      this.axios
      .get(url, { params })
      .then(res => {
        if (res) {
          this.formData = this.formatePublishFormData(res.result, 'Array');
          if(this.viewType == 'view') {
            this.$nextTick(() => {
              this.clearPlaceholders()
            })
          }
          resolve(res.result)
        } else {
          reject(res.msg)
        }
      })
      .catch(err => {
        reject(err)
      })
    })
  },
  updateFormData(v) {
    this.formData = v
  },
  setToolbarShow() {
    /**
     * 设置工具栏显示
     * @return {void}
     * @author cheng_xiaona 2019/4/24 18:07
     */
    this.toolBar.forEach(v => v.show = true)
  },
  getChuanYueData() {
    let p = JSON.parse(this.$route.query.params);
    return new Promise((resolve, reject) => {
      this.axios
        .post('docSend/openCy', p)
        .then(res => {
          if(res) {
            resolve(res.result)
          } else {
            reject(res.msg)
          }
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  init(fileType, firstCallBack, url) {
    /**
    * 初始化数据
    * @param fileType {string} 文件类型
    * @param firstCallBack {Array} init之前的回调
    * @return {void}
    * @author cheng_xiaona 2019/05/15 改
    */
    this.createLoading('正在获取数据中...');
    firstCallBack = firstCallBack || [];
    firstCallBack.forEach(v => {
      if(typeof v == 'function') {
        v()
      }
    })
    let query = this.$route.query;
    this.$set(this, 'viewType', query.viewType || 'create')
    this.$set(this, 'from', query.from || 'CaoGao')
    if(this.viewType == 'edit' || this.viewType == 'view') {
      this.formValidate = true;
      let isChuanYue = this.$route.query.isChuanYue == 1;
      (this.from != 'CaoGao' && !isChuanYue) && (this.setToolbarShow());
      return new Promise((resolve, reject) => {
        let getFormData = isChuanYue ? this.getChuanYueData : this.getFormData;
        getFormData(url)
          .then(res => {
            if(!isChuanYue) {
              this.getOpinions(res)
              getHistoryButtonState(res.originText)
                .then(data => this.historyButtonShow = data)
            } else {
              this.tempTitle = res.inSort + '传阅单';
            }
            this.formData = res
            resolve(res)
          })
          .catch(err => {
            reject()
          })
      })
    } else {
      return this.getInitInfo(fileType || 'fw');
    }
  }
}
export default methods