import getHistoryButtonState from './components/getHistoryButtonState.js';
function initConfig({components, name, data, methods, watch, created, props, computed }) {
  return {
    name: name || 'baseInfo',
    components: components || {},
    props: Object.assign({
      data: {
        type: Object,
        default: () => {
          return {}
        }
      },
      checkValidate: {
        default: false,
        type: Boolean
      },
      historyButtonShow: {
        default: false
      }
    }, props || {}),
    data () {
      return Object.assign({
        readRange: '',//传阅范围
        linkInfo: [],//关联文档
        rules: {
          title: [{ required: true, message: '请输入标题', trigger: 'blur' }],
          /*bgshgUserName: [{ required: true, message: '请选择主送领导', trigger: 'blur' }],
          hqDept: [{ required: true, message: '请选择会签部门', trigger: 'blur' }],
          zhsDept: [{ required: true, message: '请选择主送单位', trigger: 'blur' }],
          chsDept: [{ required: true, message: '请选择抄送单位', trigger: 'blur' }]*/
        },
        viewType: 'create',
        taoHongId: null,
        tempListVisible: false,
        ngDeptDialogVisible: false,
        memberDialogVisible: false,
        documentDialog: false, //关联文档
        leaderDialog: false, //主送领导dialog
        visible1: false,//主送单位dialog visible
        visible2: false,//抄送单位 dialog visible
        visible3: false, //会签部门
        isClick: false,
        centerDialogVisible: false, // office 控件
        dialogLeader:false, //
        formData: this.data,
        selectVisible1: false, // 选择单位出现下拉框
        selectVisible2: false, // 选择单位出现下拉框
        tempList: [],//套红模板列表
        dynamicTags: [],
        dynamicTags2: [],
        value: '',
        jgzData: [],
        superJgzCheckedArr: [],
        CcJgzCheckedArr: [],
        superJgz: {
          normal: {
            ids: [],
            names: []
          },
          units: {
            ids: [],
            names: []
          }
        },
        CcJgz: {
          normal: {
            ids: [],
            names: []
          },
          units: {
            ids: [],
            names: []
          }
        },
        firstFlag: true,
        canBeSelected: false,
        outWordOptions: null,
        historyDialog: false,
      }, data || {})
    },
    computed: Object.assign({
      HQData() {
        let ids = this.formData.hqDeptId;
        let res = []
        ids.forEach((v, i) => {
          res.push(JSON.stringify({ownid: v, ownname: this.formData.hqDept[i]}))
        })
        return res
      },
      isCurrent() {
        console.log(this.formData.creUserId, '|', this.$store.state.userInfo.userId)
        return this.formData.creUserId === this.$store.state.userInfo.userId
      },
      isChuanYue() {
        return this.$route.query.isChuanYue
      },
      linkHidden() {
        return this.$route.query.from == 'DaiBan' && !this.$store.state.buttons.find(v => v.num == 'SBT015')
      }
    }, computed || {}),
    mounted (){
    },
    created() {
      if(typeof created == 'function') {
        created(this);
      }
      if(this.$route.query.isChuanYue == '1') {
        this.getReadRange()
      }
      this.getJgzData();
      this.viewType = this.$route.query.viewType || 'create';

    },
    methods: Object.assign({
      getReadRange() {
        this.axios({
          method: 'POST',
          url: 'docSend/getCyfwInfo',
          data: JSON.parse(this.$route.query.params)
        })
          .then(res => {
            this.readRange = res.result
          })
      },
      inputChange(e) {
        /*this.$refs.fromData.validate(b => {
          console.log(b)
        })*/
        console.log(this.$refs)
        this.$refs.formData.validate()
      },
      handleCheckHistory() {
        /**
        * 查看历史版本
        * @return {void}
        * @author cheng_xiaona
        */
        this.historyVisible = true;
        if(!this.historyData) {
          this.axios
            .post('office/getFileVersionList', {id: this.formData.originText})
            .then(res => {
              if(res) {
                this.historyData = res.result
              }
            })
        }
      },
      hiddenJGZ(v) {
        let jgz = this.copyObj(v)
        let del = ['O_1000725', 'O_1000724'];
        del.forEach(a => {
          let i = jgz.findIndex(c => c.id == a);
          (i > -1) && (jgz.splice(i, 1));
        })
        return jgz
      },
      getLinkInfo() {
        /**
        * 获取关联文档信息
        * @return {void}
        * @author cheng_xiaona 2019/5/19 18:58
        */
        this.createLoading('正在加载中....')
        this.axios
          .post('linkInfo/findLinkInfoByRid', {id: this.formData.outId || this.formData.inId || this.formData.id})
          .then(res => {
            if(res) {
              this.linkInfo = res.result;
            }
          })
      },
      handleDeleteLinkInfo(v) {
        const d = this.formData
        this.$confirm(
          '此操作将删除该关联文档, 是否继续?',
          '提示',
          {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          })
          .then(() => {
            this.createLoading('正在删除中...')
            this.axios
              .post('linkInfo/delLinkInfoById', Object.assign(this.copyObj(v), {recordId: d.outId || d.inId || d.id}))
              .then(res =>　{
                if(res) {
                  this.$message.success(res.msg)
                  this.linkInfo = res.result
                }
              })
          })
          .catch(() => {
            this.$message({
              type: 'info',
              message: '已取消删除'
            })
          });
      },
      checkDocuments(v) {
        /**
        * 上传关联文档
        * @param v {array} 选中文档信息
        * @return {void}
        * @author cheng_xiaona 2019/5/19 17:39
        */
        this.createLoading('正在关联中....')
        this.axios
          .post('linkInfo/link', {id: this.formData.outId || this.formData.inId || this.formData.id, linkInfo: v})
          .then(res => {
            if(res) {
              this.getLinkInfo()
            }
          })
      },
      ifCanSelectCode(){
        /**
        * 是否可以编辑文号，只在待办时候调用
        * @return {void}
        * @author cheng_xiaona
        */
        let params = JSON.parse(vm.$route.query.params || "{}")
        this.axios
          .post('gzl/ifCanSelectCode', params)
          .then(res => {
            if(res && res.result) {
              this.canBeSelected = true;
              this.getArchivesCodes()
            }
          })
      },
      getArchivesCodes() {
        let userInfo = JSON.parse(localStorage.getItem('userInfo'))
        this.routeParams = JSON.parse(this.$route.query.params || '{}')
        this.axios
          .post('archivesCode/getArchivesCode', {
              id: this.routeParams.id || this.formData.id,
              codeIdY:"",
              year: this.formData.year || this.formData.taskCodeYear,
              flowType: this.formData.flowType,
              creUserDeptId: this.formData.creUserDeptId,
          })
          .then(res => {
            if(res) {
              this.outWordOptions = {};
              let o = res.result;
              for(let i in o) {
                this.outWordOptions[i] ={
                  text: o[i],
                  code: null
                }
              }
            }
          })
      },
      outWordChange(v) {
        this.getCodes(v)
          .then(res => {
            this.formData[this.$route.name == 'ShouWen' ? 'inNum' : 'outNum'] = res
          })
      },
      getCodes(code) {
        return new Promise((resolve, reject) => {
          if(this.outWordOptions[code].code) {
            resolve(this.outWordOptions[code].code)
          } else {
            if(this.cancelPromise) this.cancelPromise();
            let CancelToken = axios.CancelToken
            this.axios({
              method: 'post',
              data: {
                code,
                id: JSON.parse(this.$route.query.params || '{}').id || this.formData.id,
                year: this.formData.year || this.formData.taskCodeYear
              },
              cancel: new CancelToken(c => {
                this.cancelPromise = c
              }),
              url: 'archivesCode/getCodeNumByCode'
            })
            .then(res => {
              if(res) {
                this.outWordOptions[code].code = res.result;
                resolve(res.result)
              }
            })
          }
        })
      },
      handlePreviewDocument() {
        /**
        * 查看公文正文
        * @return {void}
        * @author cheng_xiaona 2019/4/24
        */
        window.readDoc('', this.formData.pdfId, 'YZ_ARCHIVES_FILELIST')
      },
      getTaoHongSatatus(fileId) {
        this.createLoading('加载中...')
        fileId = fileId || this.formData.pdfId
        return new Promise((resolve, reject) => {
          this.axios
            .post('office/isExist', {fileId})
            .then(res => {
              if(res.result.isExist) {
                resolve(true)
              } else {
                resolve(false)
              }
            })
            .catch(err => {
              reject(err)
            })
        })
      },
      updateHistoryButtonState() {
        getHistoryButtonState(this.formData.originText)
          .then(v => this.$emit('update:historyButtonShow', v))
      },
      handleEditDocument() {
        this.createLoading('加载中...')
        window.updateHistoryButtonState = this.updateHistoryButtonState;
        let workitemid = JSON.parse(this.$route.query.params || '{}').workitemid || '';
        let D = this.formData
        this.getTaoHongSatatus()
          .then(res => {
            if(res) {
              this.handlePreviewDocument()
            } else {
              this.getTaoHongSatatus(D.originText)
                .then(res => {
                  if(res) {
                    // window.openDoc('', D.originText, 'YZ_ARCHIVES_FILELIST', workitemid, D.subflag)
                    window.openDoc(this.formData)
                  } else {
                    this.getTemplateList()
                  }
                })
            }
          })
      },
      setTaoHong(id) {
        id = id || this.taoHongId;
        // let d = this.formData;
        // window.taoHong('', id, d.originText, d.originText, 'archives_out', d.outId, 'YZ_ARCHIVES_FILELIST', d.flowType);
        window.taoHong(id)
        this.tempListVisible = false
      },
      getTemplateList() {
        this.axios
          .post('office/getTemplateList', {fileTypeId: this.formData.flowType})
          .then(res => {
            if(res) {
              let l = res.result.length;
              let d = this.formData;
              if(l > 1) {
                this.tempList = res.result;
                this.tempListVisible = true;
              } else if(l == 1) {
                this.setTaoHong(res.result[0].modelId)
              }
            }
          })
      },
      checkedGroupChang(val, provider, index) {
        /**
        * @param val {Array} 选中的数组
        * @param provider {Object} 选填
        * @param index {String} 属性名
        * @return {void}
        * @author cheng_xiaona 2019/4/17 18:59
        */
        let ids = [],
          names = [];
        for(let i = 0; i < val.length; i++) {
          let r = this.jgzData.find(v => v.ownname == val[i])
          names.push(r.ownname)
          ids.push(r.ownid)
        }
        provider[index] = {ids, names}
      },
      setDept(val, provider) {
        /**
        * 设置主送单位和抄送单位
        * @param val {Array} 选中的数组
        * @param provider {String} 选填，Cc = 抄送单位
        * @return {void}
        * @author cheng_xiaona 2019/4/17 18:59
        */
        provider = provider || '';
        let ids = [], names = [];
        for(let i in val) {
          ids = ids.concat(val[i].ids);
          names = names.concat(val[i].names)
        }
        let dep = 'zhsDept';
        (provider == 'Cc') && (dep = 'chsDept');
        this.formData[dep] = names;
        this.formData[dep + 'Id'] = ids
      },
      handleChange(childMenu,e,i,one) {
        if ( one == 1) {
          if (e) {
            this.dynamicTags.push(childMenu);
            if ( i == 3) {
              this.selectVisible1 = true
              this.dynamicTags.splice(this.dynamicTags.indexOf(childMenu), 1)
            }
          } else {
            this.dynamicTags.splice(this.dynamicTags.indexOf(childMenu), 1);
            i == 3 ? this.selectVisible1 = false : null;
          }
        } else if ( one == 2){
          if (e) {
            this.dynamicTags2.push(childMenu);
            if ( i == 3) {
              this.selectVisible2 = true
              this.dynamicTags2.splice(this.dynamicTags2.indexOf(childMenu), 1)
            }
          } else {
            this.dynamicTags2.splice(this.dynamicTags2.indexOf(childMenu), 1);
            i == 3 ? this.selectVisible2 = false : null;
          }
        }
      },
      selectGet (value,one) {
        one == 1 ? this.dynamicTags.push(value) : this.dynamicTags2.push(value) ;
      },
      getJgzData() {
        this.createLoading('正在加载...')
        this.axios
          .get('jgzinfo/getJgzInfo')
          .then(res => {
            if(res) {
              this.jgzData = res.result
            }
          })
      }
    }, methods || {}),
    mounted() {
      this.$emit('update:baseForm', this.$refs.formData)
    },
    watch: Object.assign({
      data: {
        deep:true,
        handler(v){
          if(this.firstFlag) {
            this.firstFlag = false;
          }
          this.formData = v
        }
      },
      formData: {
        deep:true,
        handler(v){
          this.$emit('update:formData', v);
        }
      },
      checkValidate(v) {
        if(v) {

          this.$refs.formData.validate(v => {
            this.$emit('update:validate', v);
            this.$emit('update:checkValidte', false)
          })
        }
      },
      superJgzCheckedArr: {
        deep: true,
        handler(v) {
          this.checkedGroupChang(v, this.superJgz, 'normal')
        }
      },
      CcJgzCheckedArr: {
        deep: true,
        handler(v) {
          this.checkedGroupChang(v, this.CcJgz, 'normal')
        }
      },
      superJgz: {
        deep: true,
        handler(v) {
          this.setDept(v)
        }
      },
      CcJgz: {
        deep: true,
        handler(v) {
          this.setDept(v, 'Cc')
        }
      },
      $route: {
        deep: true,
        handler(v) {
          this.$destroy();
        }
      },
      firstFlag (v) {
        this.getLinkInfo()
        let isAbroadCaoGao = this.$route.name === 'abroadApply' && (this.$route.query.from == 'CaoGao' || !this.$route.query.from);
        if(this.$route.query.from == 'DaiBan') {
          this.ifCanSelectCode()
        } else if(isAbroadCaoGao) {
          this.canBeSelected = true
          this.getArchivesCodes()
        }
      }
    }, watch || {}),
  }
}
export default initConfig
