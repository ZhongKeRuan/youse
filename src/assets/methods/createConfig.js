const assign = function(provider, obj) {
  return Object.assign(provider || {}, obj || {})
}
const isFunction = function(fn) {
  return fn && fn.constructor == Function
}
function createConfig(_config) {
  _config = _config || {}
  let configItems = [
    'name',
    'components',
    'data',
    'methods',
    'watch',
    'created',
    'props',
    'computed',
    'mounted',
  ]
  return function(config) {
    config = config || {}
    let result = {}
    for(let n = 0; n < configItems.length; n++) {
      let i = configItems[n]
      switch(i) {
        case 'data':
          result[i] = function() {
            return assign(_config[i], config[i])
          }
        break;
        case 'name':
          result[i] = config[i] || _config[i] || 'Name'
        break;
        default:
          if(i == 'mounted' || i == 'created') {
            let fn1 = config[i], fn2 = _config[i];
            let fn = (isFunction(fn1) && fn1) || (isFunction(fn2) && fn2) || false;

            result[i] = (fn) || (function() {})
          } else {
            result[i] = assign(_config[i], config[i])
          }
        break
      }
    }
    return result
  }
}
export default createConfig