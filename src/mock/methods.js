const Mock = require('mockjs')
const Random = Mock.Random
const limit = 20
function createPageData (request, config, callBack) {
  let res = [];
  for(let i = 0; i < (request.limit || limit); i++) {
    let option = {};
    for(let n in config) {
      option[n] = typeof config[n] == 'function' ? config[n](i) : config[n]
    }
    res.push(option)
  }
  if(typeof callBack == 'function') {
    res = callBack(res)
  }
  return res
}
let setData = ({result, type, option}) => {
  let r = {
    code: 1,
    result,
    msg: '请求成功',
    limit,
    page: 1
  }
  switch(type) {
    case 'page':
      r = Object.assign(r, {
        count: 140,
      })
      break;
    default:
      r.result = result
      break
  }
  return r
}
function create(fn, type, config) {
  //config = {count, autoData }
  config = config || {}
  return opt => {
    let option = JSON.parse((opt && opt.body) || '{}');
    let result = fn || true
    if(typeof fn == 'function') {
      result = fn(option);
      if(type == 'page' && !config.autoData) {
        result = createPageData(option, result, config.dataCallBack)
      }
    }
    let res = setData({result, type, option});
    (config.count) && (res.count = config.count);
    // console.log(opt.url, JSON.parse((opt && opt.body) || '{}'), res)
    console.group(opt.url, JSON.parse((opt && opt.body)))
      console.log(res)
    console.groupEnd();
    return res
  }
}
function createTree({idName, childName, labelName}) {
  idName = idName || 'ID';
  childName = childName || 'children';
  labelName = labelName || 'name';
  let id = 1;
  let res = [];
  for(let n = 0; n < 10; n++) {
    let q = {};
    q[idName] = id;
    q[labelName] = Random.cname();
    if(n == 1) {
      q[childName] = [];
      for(let i = 0; i < 10; i++) {
        id++;
        let qq = {};
        qq[idName] = id;
        qq[labelName] = Random.cname();
        q[childName].push(qq)
      }
    }
    res.push(q)
    id++;
  }
  return res
}
function setLimit(opt) {
  return opt && opt.limit && opt.limit > 0 ? opt.limit : limit
}
function setFileTypeId() {
  return Random.pick([
    '3c08a9f4b7d5462a8b574b19b898a1af',
    'b87d4e6482c1477fb9d5a4f6fb945dca',
    '4850606902604b36bb2daa082f22c3ef',
    '1b3754cbdc4042de93a13e797a47c3b9',
    '368cc619570e4d8bb81c6d0e4a48dce5',
  ])
}
export {
  create,
  createTree,
  setLimit,
  setFileTypeId,
  createPageData
}