const Mock = require('mockjs')
const Random = Mock.Random;
export default {
  name() {
    return Random.cname()
  },
  text(...[start, end]) {
    if(start != void(1) && end != void(1) && !isNaN(Number(start)) && !isNaN(Number(start))) {
      start = start
      end = end
    } else {
      start = false
      end = false
    }
    console.log(start, end)
    return Random.csentence(start || 50, end || 150)
  },
  date(type) {
    type = typeof type == 'string' ? type : 'yyyy-mm-dd hh:mm:ss';
    return Random.date(type) + ''
  },
  title(...[start, end]) {
    if(start != void(1) && end != void(1) && !isNaN(Number(start)) && !isNaN(Number(start))) {
      start = start
      end = end
    } else {
      start = false
      end = false
    }
    return Random.ctitle(start || 5, end || 50)
  },
  key() {
    return Random.increment() + ''
  },
  number(...[start, end]) {
    if(start != void(1) && end != void(1) && !isNaN(Number(start)) && !isNaN(Number(start))) {
      start = start
;
      end = end
    } else {
      start = false
      end = false
    }
    return Random.integer(start || 30, end || 500)
  },
  poName() {
    return Random.pick([
      '签报',
      '白头文',
      '收文',
      '部门收文',
      '发文',
      '出国审批',
      '邀请函'
    ])
  },
  pick(arr) {
    return Random.pick(arr)
  }
}