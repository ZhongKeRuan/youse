import { create, createTree, setLimit, setFileTypeId, createPageData } from './methods.js'
import M from  './mock.js'
const Mock = require('mockjs')
const Random = Mock.Random;
Mock.setup({
  // timeout: 2000 方式一 直接设置值
  timeout: '500' // 方式二 设置区间 注意这个是一个字符串形式
})
let mocks = {
  'invitation/create': create(opt => {
    opt.id = M.key()
    return opt
  }),
  'invitation/save': create(opt => {
    return opt
  }),
  'invitation/getInfoById': create(opt => {
    return {
      id: M.key(),
      title: M.title(),
      code: '【2019】' + M.number() + '号',
      comingNum: M.number(1, 5),
      nationality: M.text(5, 10),
      comingDate: M.date(),
      reson: M.text(5, 10),
      ngDeptName: null,
      ngUserName: null,
      telephone: null,
      ngDate: null,
      stayTime: '10天'
    }
  }),
  'abroadApply/create': create(opt => {
    return {
      id: M.key(),
      title: '',
      taskCode: '中色人审字【2019】' + M.number() + '号',
      taskCodeWord: '',
      taskCodeYear: '2019',
      taskCodeNum: '',
      politicsCode: '中色人审字【2019】' + M.number() + '号',
      politicsCodeWord: '中色人审字',
      politicsCodeYear: '2019',
      politicsCodeNum: '',
      countries: '',
      reason: '',
      leaderUserName: '',
      leaderUnits: '',
      leaderPosition: '',
      leaveTime: '',
      stayTime: '',
      leaveNum: '',
      politicsNum: '',
      ngDeptName: '',
      ngDeptNameId: '',
      ngUserId: '',
      ngUserName: '',
      telePhone: '',
      ngDate: '',
      originText: '',
      sealText: '',
      pdfId: '',
      flowId: '',
      flowName: '',
      hqDeptIds: '',
      hqDeptNames: '',
      creDate: '',
      creUserId: '',
      creUserName: '',
      creDeptId: '',
      creDeptName: '',
    }
  }),
  'abroadApply/save': create(opt => opt),
  'abroadApply/getInfoById': create(opt => {
    return {
      id: M.key(),
      title: M.title(),
      taskCode: '中色人审字【2019】' + M.number() + '号',
      taskCodeWord: '',
      taskCodeYear: '2019',
      taskCodeNum: '',
      politicsCode: '中色人审字【2019】' + M.number() + '号',
      politicsCodeWord: '中色人审字',
      politicsCodeYear: '2019',
      politicsCodeNum: '',
      countries: '',
      reason: M.text(10, 50),
      leaderUserName: M.name(),
      leaderUnits: M.title(5, 10),
      leaderPosition: M.title(5, 10),
      leaveTime: M.date('yyyy-MM-dd'),
      stayTime: '10天',
      leaveNum: M.number(1, 10),
      politicsNum: M.number(1, 10),
      ngDeptName: M.title(5, 10),
      ngDeptNameId: '',
      ngUserId: '',
      ngUserName: M.title(5, 10),
      telePhone: M.number(11),
      ngDate: M.date(),
      originText: '',
      sealText: '',
      pdfId: '',
      flowId: '',
      flowName: '',
      hqDeptIds: '',
      hqDeptNames: '',
      creDate: '',
      creUserId: '',
      creUserName: '',
      creDeptId: '',
      creDeptName: '',
    }
  }),
  'office/isExist': create(opt => {
    return {isExist: false}
  }),
  'gzl/getSpecialDepartments': create(opt => {
    return {
      subFlag: ['001061028'],
      specail: ['001061028']
    }
  }),
  'office/getTemplateList': create(opt => {
    let res = []
    for(let i = 0; i< 10; i++) {
      res.push({modelName: '模板' + i, modelId: M.key()})
    }
    return res
  }),
  'idea/getToCollectIdea': create(opt => {
    let res = [];
    for(let i = 0; i < 7; i++) {
      res.push({
        idea: M.text(10, 20),
        name: M.name()
      })
    }
    return res
  }),
  'statistics/document/chart': create(opt => {
    let res = {
      title: '办公厅平均时效',
      xAxis: [],
      data: {
        '拟稿环节': [],
        '会签环节': [],
        '审批环节': [],
        '办公厅环节': []
      }
    };
    for(let index in res.data) {
      for(let i = 0; i < 12; i++) {
        res.data[index].push(M.number(10, 80))
      }
    }
    for(let i = 0; i < 12; i++) {
        res.xAxis.push(i + 1 + '月')
      }
    return res
  }),
  'statistics/document': create(opt => {
    return {
      month: i => i + 1,
      ngStep: 2.14,
      spStep: 2.14,
      hqStep: 2.14,
      bgtStep: 2.14,
    }
  }, 'page', {count: 12}),
  'statistics/groupSW/chart': create(opt => {
    let res = {
      title: '各部门处理文件数量',
      xAxis: ['办公厅','董事会办公厅','法律风控部','投资管理部','企业发展部','人事部','财务部','科学技术部','矿产勘查部','战略规划部','审计部','安全生产监督管理部','信息中心','党群工作部'],
      data: []
    };
    for(let i = 0; i < res.xAxis.length; i++){
      res.data.push(M.number(1, 100))
    }
    return res
  }),
  'statistics/groupSW': create(opt => {
    return {
      deptName: '信息部-收文',
      count: () => M.number(60,300),
      countZhuBan: () => M.number(60,300),
      countHuiQian: () => M.number(60,300),
      timeZhuBan: () => M.number(60,300),
      timeHuiQian: () => M.number(60,300),
    }
  }, 'page'),
  'statistics/groupFW/chart': create(opt => {
    let res = {
      title: '各部门处理文件数量',
      xAxis: ['办公厅','董事会办公厅','法律风控部','投资管理部','企业发展部','人事部','财务部','科学技术部','矿产勘查部','战略规划部','审计部','安全生产监督管理部','信息中心','党群工作部'],
      data: []
    };
    for(let i = 0; i < res.xAxis.length; i++){
      res.data.push(M.number(1, 100))
    }
    return res
  }),
  'statistics/groupFW': create(opt => {
    return {
      deptName: '信息部-发文',
      count: () => M.number(60,300),
      times: () => M.number(60,300),
    }
  }, 'page'),
  'statistics/memberByMonth/countChart': create(opt => {
      let res = {
        title: '处理文件数量',
        xAxis: [],
        data: []
      };
      for(let i = 0; i < 12; i++){
        res.xAxis.push(i + 1 + '月');
        res.data.push(M.number(1, 100))
      }
      return res
    }),
  'statistics/memberByMonth/timesChart': create(opt => {
    let res = {
      title: '平均处理时间',
      xAxis: [],
      data: []
    };
    for(let i = 0; i < 12; i++){
      res.xAxis.push(i + 1 + '月');
      res.data.push(M.number(1, 100))
    }
    return res
  }),
  'statistics/memberByMonth': create(opt => {
    let res = [];
    for(let i = 0; i < 12; i++) {
      res.push({
        name: M.name,
        count: M.number(60, 2000),
        times: M.number(30, 100),
        month: i + 1,
        a: 0
      })
    }
    return {
      name: M.name,
      count: i => M.number(60, 2000),
      times: i => M.number(30, 100),
      month: i => i + 1,
      a: 0
    }
  }, 'page', {count: 12}),
  'docSend/openCy': create(opt => {
    return {
      id: M.key(),
      comingDate: M.date(),
      comingCode: '[2019]xxx号',
      comingDeptName: '信息部',
      title: M.title(),
      inSort: M.poName(),
      pdfId: M.key(),
      originText: M.key(),
    }
  }),
  'statistics/members': create(opt => {
    let res = [];
    for(let i = 0; i < 10; i++) {
      res.push({
        order: i + 1,
        name: M.name,
        allCount: M.number(1, 1000),
        qbCount: M.number(1, 1000),
        btwCount: M.number(1, 1000),
        bmswCount: M.number(1, 1000),
        fwCount: M.number(1, 1000),
      })
    }
    return res
  }),
  'office/isHasVersion': create(() => true),
  'common/getFileTypes': create(opt => {
    return {
      '签报': 'b87d4e6482c1477fb9d5a4f6fb945dca',
      '白头文': '4850606902604b36bb2daa082f22c3ef',
      '收文': '1b3754cbdc4042de93a13e797a47c3b9',
      '部门收文':'368cc619570e4d8bb81c6d0e4a48dce5',
      '发文': '3c08a9f4b7d5462a8b574b19b898a1af',
    }
  }),
  'archivesOut/deleteInfoById': create(() => true),
  'search/searchAll': create(opt => {
    let res = [];
    for (let i = 0; i < opt.limit; i++) {
      res.push()
    }
    return {
      title: i => (opt.title || '') + M.title(8, 50),
      recordid: M.key,
      createTime: M.date,
      fileTypeId: setFileTypeId,

      fileTypeName: M.name,
      subFlag: i => opt.subFlag || Random.pick(['办理中', '已办结']),
      isNew: '1',
      url: '',
    }
  }, 'page'),
  'search/searchFW': create(opt => {
    return {
      title: i => (opt.title || '') + M.title(8, 50),
      recordid: M.key,
      createTime: M.date,
      fileTypeId: setFileTypeId,

      fileTypeName: M.name,
      ngDate: M.date,
      draftsmanName: M.name,
      ngDeptName: M.name,
      fwDate: M.date,
      bgshgUserName: M.name,
      zhsDept: M.name,
      chsDept: M.name,
      affixName: M.name,
      subFlag: i => opt.subFlag || Random.pick(['办理中', '已办结']),
      isNew: '1',
      wjType: opt.wjType,
      outCode: 11111
    }
  }, 'page'),
  'search/searchSW': create(opt => {
    return {
      title: (opt.title || '') + M.title(8, 50),
      recordid: M.key,
      createTime: M.date,
      fileTypeId: setFileTypeId,
      fileTypeName: M.name,
      inNum: '111',
      inSort: '111',
      comingDeptName: M.name,
      comingCode: M.name,
      comingDate: M.date,
      zsldNames: M.name,
      zhsDept: M.name,
      creUserDept: M.name,
      chsDept: M.name,
      yzryNames: M.name,
      affixName: M.name,
      isNew: '1'
    }
  }, 'page'),
  'gwtz/importQueryBook': create(),
  'gzl/reCheBan': create(),
  'gzl/ifReCheBan': create(),
  'gzl/ifShouHui': create(),
  'gzl/shouHui': create(),
  'gzl/getFlowCourse': create(opt => {
    return [{
        "index": "1",
        "wflevename": "起草人",
        "receivetime": "2019-04-28 10:57:43",
        "sendtime": "2019-04-28 10:57:43",
        "staytime": "小于1分钟",
        "sendname": "刘瑾/规划管理处",
        "idea": "",
        "phone": "111111"
      },
      {
        "index": "2",
        "wflevename": "其他人员",
        "receivetime": "2019-04-28 10:57:43",
        "sendtime": "2019-04-28 10:58:17",
        "staytime": "小于1分钟",
        "sendname": "刘瑾/规划管理处",
        "idea": "拟稿环节其它人员意见",
        "phone": "111111"
      }
    ]
  }),
  'sent/getSentByPage': create(opt => {
    let o = {
      "id": M.key,
      "recordId": M.key,
      "title": M.title,
      "creDate": M.date,
      "flowType": setFileTypeId,
      "isNew": "1",
      poName: M.poName
    }
    return o
  }, 'page'),
  'sent/getSentUrl': create(''),
  'gzl/cheBan': create(''),
  'idea/getCommonIdea': create(opt => {
    return [{
      value: "不同意",
      id: 0
    }]
  }),
  'common/getSessionInfo': create(opt => {
    return {
      "userId": "013333",
      "userName": "刘瑾",
      "deptId": M.pick(["001061028", '2222']),
      "deptName": '财务部',
      isShow: true
    }
  }),
  'common/changeDept': create(),
  'resource/getResource': create((opt) => {
    let res = [];
    let m = ['协同工作', '公文管理', '事务审批', '公共信息', '个人空间']
    let sm = [
      ['新建协同', '待发协同', "新建协同", "待发协同", "已发协同", "待办协同", "已办协同"],
      ["发文", "收文", "公文查询", "公文台账"],
      ["新建模板", "草稿列表", "待办列表", "已办列表", "已发列表", "已阅列表", "待阅列表"],
      ["分类信息", "通讯录", "文档中心"],
      ["工作代理", "个人文件柜", "个人设置", "自定义机构组", "自定义人员组"]
    ];
    let json = {
      modelUrl: 'http://baidu.com',
      blank: 0,
      icon: 'el-icon-menu'
    }
    for (let i = 0; i < m.length; i++) {
      let q = Object.assign(JSON.parse(JSON.stringify(json)), { modelId: '000' + M.key(), modelName: m[i] });
      let arr = [];
      for (let ii = 0; ii < sm[i].length; ii++) {
        let sq = Object.assign(JSON.parse(JSON.stringify(json)), { modelId: '000' + M.key, modelName: sm[i][ii] });
        (sq.modelName == '公文台账') && (sq.modelUrl = '#/newWindow/gongWenTaizhang');
        arr.push(sq);
      }
      arr.push({
        modelName: '文统计',
        modelUrl: '#/newWindow/statistics/document',
        isNew: '1'
      })
      arr.push({
        modelName: '集团统计-发文',
        modelUrl: '#/newWindow/statistics/group?documentType=FaWen',
        isNew: '1'
      })
      arr.push({
        modelName: '集团统计-收文',
        modelUrl: '#/newWindow/statistics/group',
        isNew: '1'
      })
      arr.push({
        modelName: '人员统计',
        modelUrl: '#/newWindow/statistics/member',
        isNew: '1'
      })
      arr.push({
        modelName: '单个人员',
        modelUrl: '#/newWindow/statistics/memberByMonth',
        isNew: '1'
      })
      arr.push({
        modelName: '三级菜单',
        modelUrl: '',
        isNew: '1',
        modelId: '000' + M.key,
        sonResourceList: [{
          modelName: '三级菜单-1',
          modelUrl: '',
          isNew: '1',
          modelId: '000' + M.key,
        }]
      })
      q.sonResourceList = arr;
      res.push(q)
    }
    /*let _res = JSON.parse(JSON.stringify(res))
    res = res.concat(_res)*/
    return res
  }),
  'archivesOut/getInfoById': create(opt => {
    return {
          outId: M.key(),
          outCode: M.key(),
          year: M.number(1, 5) + '年',
          outNum: M.key(),
          needGrade: '0',
          secretGrade: '0',
          secretDate: M.date(),
          outType: '0',
          title: M.title(),
          bgshgUserId: '0',
          bgshgUserName: '高丽',
          hqDeptId: '1013,1014,1017',
          hqDept: '内部单位1,外部单位,子节点1',
          draftsmanName: M.name(),
          ngDeptName: M.name(),
          ngDeptId: M.key(),
          draftsmanId: M.key(),
          ngDate: M.date(),
          fwDate: M.date(),
          // zhsDeptId: '1013,1014,1017',
          // zhsDept: '内部单位1,外部单位,子节点1',
          zhsDeptId: '',
          zhsDept: '',
          chsDeptId: M.key(),
          chsDept: M.name(),
          originText: M.key(),
          affixId: M.key(),
          affixName: M.name(),
          memo: M.name(),
          flowType: M.key(),
          contactUser: M.name(),
          contactPhone: '12545445',
          subflag: null,
          trueText: null,
          linkSignId: M.key(),
          creDate: null,
          creUserId: '013333',
          creUserName: null,
          creUserDeptId: M.key(),
          creUserDeptName: null
        }
  }),
  'gzl/encodeInfo': create(opt => {
    let res = {};
    if (opt.flag == 'selectoption') {
      res = {
        exception: '',
        flag: 'selectdept',
        node: [{
            "isMuch": "false",
            "nextWfleveType": "3",
            "method": "",
            "nextWfleveId": "1555120506793",
            "opCond": "",
            "name": "会签部门",
            "memo": "",
            "isSelect": "true",
            "disabled": false,
            "id": "1555417386007",
            "wfleveId": "1555000795878",
            "fileTypeId": "79131b0f966f4107a006206e23495d93"
          },
          {
            "isMuch": "false",
            "nextWfleveType": "3",
            "method": "",
            "nextWfleveId": "1555453099553",
            "opCond": "",
            "name": "noName",
            "memo": "",
            "isSelect": "true",
            "disabled": false,
            "id": "1555502718516",
            "wfleveId": "1555000795878",
            "fileTypeId": "79131b0f966f4107a006206e23495d93"
          }
        ]
      }
    } else if (opt.flag == 'selectdept') {
      res = {
        "exception": "",
        "node": [{
          "flag": "",
          "method": "",
          "examinetag": [{
              type: 4,
              name: '共同办',
              selected: 'true'
            },
            {
              type: 5,
              name: '同时办',
              selected: 'true'
            },
            {
              type: 6,
              name: '顺序办',
              selected: 'true'
            }
          ],
          "memo": "",
          "wfleveId": "1555762320131",
          "fileTypeId": "3c08a9f4b7d5462a8b574b19b898a1af",
          "type": "1",
          "remind": {
            "name": "短信",
            "type": "1",
            "selected": "true"
          },
          "nodes": [
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                  "name": "史捷",
                  "deptId": "001061016001",
                  "id": "8a80949f5e8dd5c9015e8dea70000026",
                  "type": "user",
                  "selected": true,
                },
                {
                  "name": "史捷fei",
                  "deptId": "001061016001",
                  "id": "8a80949f5e83dd5c9015e8dea70000026",
                  "type": "user",
                  "selected": true,
                }
              ],
              "name": "综合服务处",
              "id": "001061016001",
              "selected": true
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "张格格",
                "deptId": "001061008006",
                "id": "ff8080815c10af6f015c112c329d003d",
                "type": "user",
                "selected": true,
              }],
              "name": "综合处",
              "id": "001061008006",
              "selected": true
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "张力月",
                "deptId": "001061005006",
                "id": "ff8080815c10af6f015c14bca65800b8",
                "type": "user",
                "selected": false,
              }],
              "name": "综合处",
              "id": "001061005006",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "李祎",
                "deptId": "001061007005",
                "id": "8a80949f5e8dd5c9015e8de897d70017",
                "type": "user",
                "selected": false,
              }],
              "name": "审计一处",
              "id": "001061007005",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "吴琳",
                "deptId": "001061020002",
                "id": "8a8782e9574258f40157427d535800f0",
                "type": "user",
                "selected": false,
              }],
              "name": "审理处",
              "id": "001061020002",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "于萌萌",
                "deptId": "001061006007",
                "id": "8a8782e9574258f40157429b317f02e0",
                "type": "user",
                "selected": false,
              }],
              "name": "人事调配处",
              "id": "001061006007",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "陈晨",
                "deptId": "001061015001",
                "id": "8a8782e9574258f4015742803fcc0111",
                "type": "user",
                "selected": false,
              }],
              "name": "投资并购处",
              "id": "001061015001",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "赵丹妮",
                "deptId": "001061011003",
                "id": "8a80949f602b40b401602e69c4780090",
                "type": "user",
                "selected": false,
              }],
              "name": "政研处",
              "id": "001061011003",
              "selected": false
            },
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "严佳琪",
                "deptId": "001061003003",
                "id": "ff8080815c1592b5015c15cdec780039",
                "type": "user",
                "selected": false,
              }],
              "name": "规划管理处",
              "id": "001061003003",
              "selected": false
            }
          ],
          "nextWfleveId": "1555965061069",
          "opCond": "",
          "name": "送会签部门综合助理",
          "limit": {},
          "id": "1555941552177",
          "selected": false
        }],
      }
      if(opt.node[0].name == 'noName') {
        res = {
        "exception": "",
        "node": [{
          "flag": "",
          "method": "",
          "examinetag": [{
              type: 4,
              name: '共同办',
              selected: 'true'
            },
            {
              type: 5,
              name: '同时办',
              selected: 'true'
            },
            {
              type: 6,
              name: '顺序办',
              selected: 'true'
            }
          ],
          "memo": "",
          "wfleveId": "1555762320131",
          "fileTypeId": "3c08a9f4b7d5462a8b574b19b898a1af",
          "type": "1",
          "remind": {
            "name": "短信",
            "type": "1",
            "selected": "true"
          },
          "nodes": [
            {
              "deptType": "02",
              "type": "dept",
              "nodes": [{
                "name": "严佳琪",
                "deptId": "001061003003",
                "id": "ff8080815c1592b5015c15cdec780039",
                "type": "user",
                "selected": false,
              }],
              "name": "规划管理处",
              "id": "001061003003",
              "selected": false
            }
          ],
          "nextWfleveId": "1555965061069",
          "opCond": "",
          "name": "送会签部门综合助理",
          "limit": {},
          "id": "1555941552177",
          "selected": false
        }],
      }
      }
    } else {
      res = {
        "exception": "",
        node: [{

          name: "送部门综合助理",
          receiveName: "下一办理环节"
        }]
      }
    }
    return res
  }),
  'jgzinfo/getOneJgzInfo': create(opt => {
    return [
      {
        ownid: M.key(),
        ownname: '机构组',
        isLeaf: false,
        children: [
          {
            ownid: '1017',
            ownname: '子节点1',
            isLeaf: true,
          },
          {
            ownid: M.key(),
            ownname: '子节点2',
            isLeaf: true,
          }
        ]
      }
    ]
  }),
  'backlog/getBackLogByUserId': create(opt => { // 待办接口
    let o = {
      deptId: M.key,
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick(['紧急', '特急', '普通']),
      id: M.key,
      url: 'http://baidu.com',
      //url: '',
      isNew: '1', // 为1时为跳转新建的 0为之前的跳转返回url
      code: 'hj=普通,isHaveAffix=0,otherContent=',
      typeSort: 'no', // flow 旧数据拼接url
    }
    return o
  },
  'page'),
  'backlog/getFlowTypeUrl': create(opt => 'https://segmentfault.com'),
  'backlog/getBackLogByPage': create(opt => { // 待办更多分页
    let o = {
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick['急', '特急'],
      id: M.key,
      url: '',
      isNew: '1', // 为1时为跳转新建的 0为之前的跳转返回url
      code: 'hj=普通,isHaveAffix=0,otherContent=',
      typeSort: 'flow'
    }
    return o
  }, 'page'),
  'message/getMoreMessageList': create(opt => { // 待办更多分页
    let o = {
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick['急', '特急'],
      id: M.key,
      url: '',
      isNew: '1', // 为1时为跳转新建的 0为之前的跳转返回url
      code: 'hj=普通,isHaveAffix=0,otherContent=',
      typeSort: 'flow'
    }
    return o
  }, 'page'),
  'message/getMessageList': create(opt => { // 待办更多分页
    let o = {
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick['急', '特急'],
      id: M.key,
      url: '',
      isNew: '1', // 为1时为跳转新建的 0为之前的跳转返回url
      code: 'hj=普通,isHaveAffix=0,otherContent=',
      typeSort: 'flow'
    }
    return o
  }, 'page'),
  'docSend/getCyfwInfo': create(opt => '1231465'),
  'pendread/getPendReadInfo': create(opt => { // 首页待阅
    let o = {
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date ,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick['急', '特急'],
      id: M.key,
      url: 'https://www.jd.com/',
      isNew: "1",
      cyType: '传阅'
    }
    return o
  }, 'page'),
  'pendread/getPendReadInfoByPage': create(opt => { // 待阅更多
    let o = {
      title: M.title,
      fileTypeId: setFileTypeId,
      recordId: M.key,
      preUserId: M.key,
      preUserName: M.name,
      preTime: M.date,
      userId: M.key,
      poName: M.poName,
      hj: i => Random.pick['急', '特急'],
      id: M.key,
    }
    return o
  }, 'page'),
  'finished/getFinishedByUserId': create(opt => {
    let o = {
      fileTypeId: setFileTypeId,
      outId: M.key,
      id: M.key,
      fileTypeName: M.name,
      recordId: M.key,
      title: M.title,
      preTime: M.date,
      userId: M.key,
      code: i => Random.string('number', 4, 4),
      poName: M.poName,
      url: 'http://baidu.com',
      isNew: '1'
    }
    return o
  }, 'page'),
  'finished/getFinishedByPage': create(opt => {
    let o = {
      fileTypeId: setFileTypeId,
      fileTypeName: M.name,
      recordId: M.key,
      title: M.title(8, 50),
      preTime: i => M.date(),
      userId: M.key,
      code: i => Random.string('number', 4, 4),
      poName: M.poName,
      url: 'http://baidu.com',
    }
    return o
  }, 'page'),
  'afterread/getAfterReadInfo': create(opt => {
    let o = {
      keyWord: '收文',
      sendTitle: M.title,
      creTime: M.date,
      viewUrl: 'http://baidu.com',
      poName: M.poName
    }
    return o
  }, 'page'),
  'afterread/getAfterReadInfoByPage': create(opt => {
    let o = {
      keyWord: '收文',
      sendTitle: M.title(8, 50),
      creTime: M.date + ' ' + Random.time(),
      viewUrl: 'http://baidu.com'
    }
    return o
  }, 'page'),
  'archivesOut/ifShowFf': create(),
  'office/getFileVersionList': create(() => {
    return {
      "createTime": M.date,
      "createUserId": M.key,
      "createUserName": M.name,
      "id": M.key,
      "mainTableId": M.key,
      "textAddress": "/file/archives/12aad96f370b4759b8c604cafa3aaf83/1559462707090/12aad96f370b4759b8c604cafa3aaf83.docx",
      "userDeptName": "规划管理处",
      "version": 1,
    }
  }),
  'archivesOut/create': create(opt => {
    return {
      draftsmanId: M.key(),
      draftsmanName: M.name(),
      ngDate: M.date(),
      outId: M.key(),
      // ifTsfw: M.pick(['0', '1'])
      ifTsfw: '1'
    }
  }),
  'jgzinfo/getJgzInfo': create(opt => {
    let res = [];
    for (let i = 0; i < 6; i++) {
      let o = {
        ownid: M.key(),
        ownname: M.name() + '单位'
      }
      res.push(o)
    }
    return res
  }),
  'jgzinfo/getDwInfo': create(opt => {
    let res = [
      {
        ownname: '内部单位',
        ownid: '1013'
      }
    ];
    for (let i = 0; i < 6; i++) {
      let o = {
        ownid: M.key(),
        ownname: M.name() + '单位'
      }
      res.push(o)
    }
    return res
  }),
  'jgzinfo/getWbdwInfo': create(opt => {
    let res = [
      {
        ownname: '外部单位',
        ownid: '1014'
      }
    ];
    for (let i = 0; i < 6; i++) {
      let o = {
        ownid: M.key(),
        ownname: M.name() + '单位'
      }
      res.push(o)
    }
    return res
  }),
  'jgzinfo/getCzqyInfo': create(opt => {
    let res = [
      {
        ownname: '出资企业',
        ownid: '1015'
      }
    ];
    for (let i = 0; i < 6; i++) {
      let o = {
        ownid: M.key(),
        ownname: M.name()
      }
      res.push(o)
    }
    return res
  }),
  'jgzinfo/getChildrenBySend': create(opt => {
    let res = [
      {
        ownname: '高丽',
        ownid: '0'
      }
    ];
    for (let i = 0; i < 6; i++) {
      let o = {
        ownid: M.key(),
        ownname: M.name(),
        isLeaf: Random.pick([true, false])
      }
      res.push(o)
    }
    return res
  }),
  'archivesOut/save': create(opt => {

    return Object.assign(opt, {
      outId: M.key(),
      flowType: M.key(),
      id: M.key(),
      creUserDeptId: M.key(),
      creUserId: '013333',
      originText: M.key()
    })
  }),
  'jgzinfo/getChildrenNodes': create(opt => {
    return [{
      "isLeaf": false,
      "deptId": "001061",
      "deptName": "中国有色集团总部",
      "id": "O_001061",
      "modeltype": "",
      "ownid": "001061",
      "ownname": "中国有色集团总部",
      "owntype": "flow_organize",
      "pid": "0",
      "url": ""
    }]
  }),
  'jgzinfo/getNowDeptInfo': create(opt => {
    return [{
      "isLeaf": false,
      "deptId": "001061",
      "deptName": "信息部",
      "id": "O_001061",
      "modeltype": "",
      "ownid": "001061",
      "ownname": "信息部",
      "owntype": "flow_organize",
      "pid": "0",
      "url": ""
    }]
  }),
  'docSend/chuanYue': create(opt => '知会成功！'),
  'news/getNewsInfo': create(opt => { // 首页通知公告 zhang_yanhong
    let res = [];
    for (let i = 0; i < 5; i++) {
      res.push(o)
    }
    let o = {
      id: M.key,
      title: M.title,
      issueTime: M.date,
      showflag: i => Random.pick(['01'])
    }
    return o
  }, 'page'),
  'news/getNewsInfoByPage': create(opt => { // 通知公告列表 更多 zhang_yanhong
    let o = {
      id: M.key,
      title: M.title,
      issueTime: M.date,
      showflag: i => Random.pick(['01'])
    }
    return o
  }, 'page'),
  'idea/getIdea': create(opt => { // 获取正式意见 zhang_yanhong
    let res = [];
    let idealist = [];
    for (let i = 0; i < 5; i++) {
      let o = {
        idea: M.title(),
        ideatime: M.date(),
        username: M.name()
      }
      idealist.push[o]
    }
    for (let i = 0; i < 5; i++) {
      let o = {
        id: M.key(),
        ideaList: idealist,
        name: Random.pick(['JTLD_SP_YJ', 'JTLD_SH_YJ', 'BGT_LD_YJ', 'HQHJ_NGR_YJ', 'NGHJ_QTRY_YJ']) // 领导审批 领导审核 办公厅审核意见 会签部门意见 主办部门意见
      }
      res.push = [o]
    }
    return res
  }),
  'archivesIn/create': create(opt => {
    return {
      creUserId: '013333',
      creUserName: M.name(),
      creDate: M.date(),
      creUserDeptId: M.key(),
      creUserDeptName: M.name(),
      isNew: 1,
      bmswType: '交办',
      inId: M.key()
    }
  }),
  'archivesOut/getCgInfo': create(opt => {
    let o = {
      "outId": M.key,
      "title": i => M.title(10, 50),
      "flowType": i => Random.pick(['3c08a9f4b7d5462a8b574b19b898a1af', 'b87d4e6482c1477fb9d5a4f6fb945dca', '4850606902604b36bb2daa082f22c3ef', '1b3754cbdc4042de93a13e797a47c3b9']),
      "outCode": '',
      "year": '',
      "outNum": '',
      "draftsmanId": '',
      "draftsmanName": '',
      "ngDeptId": '',
      "ngDeptName": '',
      "ngDate": '',
      "fwDate": '',
      "secretGrade": '',
      "needGrade": '',
      "secretDate": '',
      "outType": '',
      "bgshgUserId": '',
      "bgshgUserName": '高丽',
      "zhsDeptId": '',
      "zhsDept": '',
      "chsDeptId": '',
      "chsDept": '',
      "hqDeptId": '',
      "hqDept": '',
      "contactUser": '',
      "contactPhone": '',
      "memo": '',
      "subflag": '',
      "originText": '',
      "trueText": '',
      "pdfId": '',
      "affixId": '',
      "affixName": '',
      "linkInId": '',
      "linkSignId": '',
      "creDate": "2019-04-28 18:07:26",
      "creUserId": '013333',
      "creUserName": '',
      "creUserDeptId": '',
      "creUserDeptName": '',
      "wjType": '',
      "isNew": '',
      "fwDept": '',
      "outWord": '',
      isNew: '1',
      poName: M.poName
    }
    return o
  }, 'page'),
  'archivesIn/save': create(opt => {
    return {
      inId: M.key(),
      title: opt.title,
      flowType: M.key(),
      inNum: '',
      needGrade: '',
      secretGrade: '',
      saveLimitName: '',
      comingDeptId: '',
      comingDeptName: '',
      comingDate: '',
      comingCode: '',
      inSort: '',
      zsldIds: '0',
      zsldNames: '高丽',
      zhsDeptId: '1013,1014,1017',
      zhsDept: '内部单位,外部单位,出资企业',
      chsDeptId: '',
      chsDept: '',
      xbdwIds: '',
      xbdwNames: '',
      yzryIds: '',
      yzryNames: '',
      ifChs: opt.ifChs,
      ifSb: '',
      finishLimit: '',
      sbDemind: '',
      memo: '',
      trueText: '',
      acceptDate: '',
      acceptDeptId: '',
      acceptDeptName: '',
      acceptUserId: '',
      acceptUserName: '',
      subFlag: '',
      creDate: '',
      creUserId: '013333',
      creUserName: '',
      creUserDeptId: '',
      creUserDeptName: '',
      isNew: '1'
    }
  }),
  'docSend/banJie': create(opt => {
    return 1
  }),
  'archivesIn/getInfoById': create(opt => {
    return {
      // ifYb: Random.pick('1', ''),
      ifYb: 1,
      inId: M.key(),
      title: M.title(),
      flowType: M.key(),
      inNum: '',
      needGrade: '1213432',
      secretGrade: '',
      saveLimitName: '',
      comingDeptId: '',
      comingDeptName: '',
      comingDate: M.date(),
      comingCode: '',
      inSort: '',
      zsldIds: '0',
      zsldNames: '高丽',
      zhsDeptId: '1013,1014,1017',
      zhsDept: '内部单位,外部单位,出资企业',
      chsDeptId: M.key(),
      chsDept: M.name(),
      xbdwIds: M.key(),
      xbdwNames: M.name(),
      yzryIds: '',
      yzryNames: '',
      yfileTypeId: setFileTypeId(),
      ifChs: opt.ifChs,
      ifSb: '',
      finishLimit: '',
      sbDemind: '',
      memo: '',
      trueText: '',
      acceptDate: '',
      acceptDeptId: '',
      acceptDeptName: '',
      acceptUserId: '',
      acceptUserName: '',
      subFlag: '',
      creDate: M.date(),
      creUserId: '013333',
      creUserName: '',
      creUserDeptId: '',
      creUserDeptName: '',
      isNew: '1',
      bmswType: '交办',
      yinId: 'yInId',
      year: M.date('yyyy')
    }
  }),
  'gzl/ifCanSelectCode': create(),
  'archivesCode/getCodeNumByCode': create(opt => M.key()),
  'archivesCode/getArchivesCode': create(opt => {
    let res = {}
    for (let i = 0; i < 8; i++) {
      res[M.key()] = M.name()
    }
    return res
  }),
  'idea/getIdea': create(() => {
    let res = [];
    for (let i = 0; i < 2; i++) {
      let ideaList = [];
      for (let n = 0; n < 2; n++) {
        let username = M.name
        let o = {
          idea: Random.cparagraph(10),
          username,
          imageUrl: Random.pick(['', Random.image('200x100', '#02adea', username)]),
          ideatime: '1111',
          iscollect: Random.pick([1, 0]),
          deptname: M.name() + '部门'
        };
        ideaList.push(o)
      }
      let o = {
        deptName: M.name() + '部门名称',
        note: M.title(5) + '环节',
        ideaList
      }
      res.push(o)
    }
    return res
  }),
  'affix/getAffixList': create(opt => {
    let res = [];
    for (let i = 0; i < 5; i++) {
      let o = {
        url: '111',
        name: M.name() + '.' + Random.pick(['doc', 'pdf']),
        id: M.key(),
        hasVersion: Random.pick([true, false]),
        creUserName: M.name(),
        creUserDeptName: M.name() + '部门'
      }
      res.push(o)
    }
    return res
  }),
  'gzl/getButton': create(opt => {
    return [
      { num: 'SBT005', buttonname: '签章' },
      { num: 'SBT006', buttonname: '分发' },
      { num: 'SBT009', buttonname: '分发' },
      { num: 'SBT014',buttonname: '提取意见'},
      { num: 'SBT011',buttonname: '交办'},
      { num: 'SBT010',buttonname: '办结'},
      // { num: 'SBT016',buttonname: '上传'},
      // { num: 'SBT015',buttonname: '可编辑按钮'},
    ]
  }),
  'pendread/getSendReadInfo': create(opt => {
    let o = {
      orderNum: i => i,
      sendUser: M.title,
      inDeptName: M.title,
      inUser: M.name,
      creTime: M.date,
      seeFlag: i => Random.pick([0, 1])
    }
    return o
  }, 'page'),
  'docSend/yueBi': create(opt => true),
  'archivesOut/getFfjlInfo': create(opt => {
    let o = {
      state: '状态',
      title: M.title,
      sendDept: M.date,
      fwDate: M.date,
      acceptUser: M.name,
      backDate: M.date,
      orderNum: i => i
    }
    return o
  }, 'page'),
  'archivesIn/findBljgHz': create(opt => {
    let res = [];
    for (let i = 0; i < 10; i++) {
      res.push({
        acceptDeptName: M.name,
        result: Random.cparagraph(Random.pick([10, 100, 5]))
      })
    }
    return res
  }),
  'linkInfo/delLinkInfoById': create(opt => {
    let res = [];
    for (let i = 0; i < opt.limit; i++) {
      res.push({
        orderNum: '测试',
        recordId: '测试',
        fileTypeId: setFileTypeId,
        fwDate: '测试',
        code: '测试',
        title: '测试',
        draftsManName: '测试',
        ngDeptName: '测试',
        hqDept: '测试',
        ngDate: '测试',
        subFlag: '测试',
        needGrade: '测试',
        zhsDept: '主送单位'
      })
    }
    return res
  }, 'page'),
  'linkInfo/link': create(),
  'gwtz/findOutGwtz': create(opt => {
    return {
      orderNum: '测试',
      recordId: '测试',
      fileTypeId: setFileTypeId,
      fwDate: '测试',
      code: '测试',
      title: '测试',
      draftsManName: '测试',
      ngDeptName: '测试',
      hqDept: '测试',
      ngDate: '测试',
      subFlag: '测试',
      needGrade: '测试',
      zhsDept: '主送单位',
      isNew: '1',
      poName: M.poName
    }
  }, 'page'),
  'gwtz/findInGwtz': create(opt => {
    return {
      orderNum: '收文测试',
      recordId: '收文测试',
      fileTypeId: setFileTypeId,
      comingDate: '收文测试',
      code: '收文测试',
      title: '收文测试',
      comingDeptName: '收文测试',
      zhsDept: '收文测试',
      xbdwNames: '收文测试',
      subFlag: '收文测试',
      needGrade: '收文测试',
    }
  }, 'page'),
  'linkInfo/findLinkInfoByRid': create(opt => {
    let res = [];
    for(let i = 0; i < 10; i++) {
      res.push({
        id: M.key(),
        recordId: M.key(),
        poName: Random.pick(['发文', '收文', '白头文', '签报']),
        fileTypeId: setFileTypeId(),
        isNew: '1',
        linkName: M.title(8, 50)
      })
    }
    return res
  }),
  'xjmb/findMoreXjmb': create(opt => {
    let res = [];
    for (let i = 0; i < 6; i++) {
      let p = {
        "tabName": "公文审批" + i,
        list: []
      };
      for (let n = 0; n < 20; n++) {
        p.list.push({
          name: M.title(10),
          url: Random.pick(['/newWindow/Publish', '/newWindow/QianBao', '/newWindow/BaiTouWen', '/newWindow/ShouWen'])
        })
      }
      res.push(p)
    }
    return res
  }),
  'xjmb/findCyXjmb': create(() => {
    let res = []
    for (let n = 0; n < 20; n++) {
      res.push({
        name: M.title(10),
        url: Random.pick(['/newWindow/Publish', '/newWindow/QianBao', '/newWindow/BaiTouWen', '/newWindow/ShouWen']),
        isNew: '1'
      })
    }
    return res
  }),
  'idea/createNewIdea': create(),
  'idea/deleteIdea': create(),
  'idea/sortIdeas': create(),
}
for (let url in mocks) {
  Mock.mock(RegExp(url + ".*"), /post|get/i, mocks[url])
}