import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
let router = new Router({
  // mode: process.env.NODE_ENV == 'development' ? 'hash' : 'history',
  routes: [
    {
      path: '/',
      alias: '/home',
      name: 'home',
      component: () => import('./views/Home/Home.vue')
    },
    {
      path: '/workStream',
      name: 'workStream',
      component: () => import('./views/workStream/index.vue')
    },
    {
      path: '/newWindow',
      name: 'newWindow',
      component: () => import('./views/newWindow/newWindow.vue'),
      children: [
        {
          path: 'GongWenTaiZhang',
          name: 'GongWenTaiZhang',
          component: () => import('./views/GongWenTaiZhang/index.vue')
        },
        {
          path: 'publish',
          name: 'Publish',
          component: () => import('./views/publish/publish.vue')
        },
        {
          path: 'ShouWen',
          name: 'ShouWen',
          component: () => import('./views/ShouWen/ShouWen.vue')
        },
        {
          path: 'QianBao',
          name: 'QianBao',
          component: () => import('./views/QianBao/QianBao.vue')
        },
        {
          path: 'BaiTouWen',
          name: 'BaiTouWen',
          component: () => import('./views/BaiTouWen/BaiTouWen.vue')
        },
        {
          path: 'TestProcess',
          name: 'TestProcess',
          component: () => import('./views/test/process.vue')
        },
        {
          path: 'office',
          name: 'office',
          component: () => import('./views/test/office.vue')
        },
        {
          path: 'More',
          name: 'More',
          component: () => import('./views/More/More.vue')
        },
        {
          path: 'statistics',
          redirect:'statistics/group',
          name: 'statistics',
          component: () => import('./views/statistics/index.vue'),
          children: [
            {
              path: 'group',
              name: 'group',
              component: () => import('./views/statistics/group.vue'),
            },
            {
              path: 'member',
              name: 'member',
              component: () => import('./views/statistics/member.vue'),
            },
            {
              path: 'memberByMonth',
              name: 'memberByMonth',
              component: () => import('./views/statistics/memberByMonth.vue'),
            },
            {
              path: 'document',
              name: 'document',
              component: () => import('./views/statistics/document.vue'),
            }
          ]
        },
        {
          path: 'invite',
          name: 'Invite',
          component: () => import('./views/invite/index')
        },
        {
          path: 'abroadApply',
          name: 'abroadApply',
          component: () => import('./views/abroadApply/index')
        },
      ]
    },
    {
      path: '/Search',
      name: 'Search',
      component: () => import('./views/Search/Search.vue')
    }
  ]
})
router.afterEach((to, from) => {
  let L = window.vm.$store.state.loading
  if(L) L.close();
})
/*router.beforeEach((to, from, next) => {
  Vue.axios
    .get('system/loginAction.do?method=loginProcess&username=019529&userPassword=1')
    .catch(err => {
      next()
    })
})*/
router.afterEach((to, from) => {
  // ...
  vm.$store.commit('SET_FROM', from.name)
})
export default router
